import moment from 'moment';

const queryString = params =>
  Object.keys(params)
    .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(params[key])}`)
    .join('&');

/**
 * {@link API docs: http://openbiomaps.org/documents/api.html}
 * @example <caption>Usage of the Client class</caption>
const client = new Client({ apiPrefix: 'http://openbiomaps.org' });

(async () => {
  const authObject = await client.auth({
    username: 'email@email.com',
    password: 'abcdef123456',
  });

  const forms = await client.getFormList(authObject);
  console.log('forms', forms);

  const formParams = {
    accessToken: authObject.accessToken,
    id: 113,
  };
  const form = await client.getFormData(formParams);
  console.log('form', form);
})();
 */
export default class Client {
  constructor({
    apiPrefix,
  }) {
    this.apiPrefix = apiPrefix;
  }

  set prefix(prefix) {
    this.apiPrefix = prefix;
  }
  /**
   * Authenticates the client.
   *
   * @example curl -u mobile:123 http://openbiomaps.org/oauth/token.php -d "grant_type=password&username=email@email.com&password=****&scope=get_form_data+get_form_list+put_data"
   * @returns {void}
   */
  async auth({
    username,
    password,
  }) {
    const path = 'oauth/token.php';
    const scope = 'get_form_data get_form_list put_data get_trainings get_training_questions training_results training_toplist';
    try {
      console.log('scope: ', scope);
      console.log(`${this.apiPrefix}/${path}`);
      const requestTime = +new Date();
      const res = await fetch(`${this.apiPrefix}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          // mobile:123 in base64 coding
          Authorization: 'Basic bW9iaWxlOjEyMw==',
        },
        body: queryString({
          grant_type: 'password',
          username,
          password,
          scope,
        }),
      });

      const json = await res.json();
      if (json.error) {
        throw json;
      }

      const authObject = {
        accessToken: json.access_token,
        expiresIn: json.expires_in * 1000,
        refreshToken: json.refresh_token,
        requestTime,
      };

      console.log('got auth object', authObject);
      return authObject;
    } catch (e) {
      throw e;
    }
  }

  /**
   * Refresh the client auth info.
   */
  async refresh({
    refreshToken,
  }) {
    const path = 'oauth/token.php';

    try {
      const requestTime = +new Date();
      const res = await fetch(`${this.apiPrefix}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
          // mobile:123 in base64 coding
          Authorization: 'Basic bW9iaWxlOjEyMw==',
        },
        body: queryString({
          grant_type: 'refresh_token',
          refresh_token: refreshToken,
        }),
      });

      const json = await res.json();

      if (json.error) {
        throw new Error(JSON.stringify(json));
      }

      const authObject = {
        accessToken: json.access_token,
        expiresIn: json.expires_in * 1000,
        refreshToken: json.refresh_token,
        requestTime,
      };

      console.log('got auth object', authObject);
      return authObject;
    } catch (e) {
      throw new Error(`API error: unable to authenticate ${e}`);
    }
  }


  /**
   * List the servers
   */
  getServerList = async () => {
    const path = 'http://openbiomaps.org/projects/openbiomaps_network/?query_api={"available":"up"}&output=json&filename=';

    try {
      const res = await fetch(`${path}`, {
        method: 'GET',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
      });

      const json = await res.json();

      if (json.error) {
        throw new Error(JSON.stringify(json));
      }

      console.log('got server object', json);
      return json;
    } catch (e) {
      throw new Error(`API error: unable to authenticate ${e}`);
    }
  }

  /**
   * Lists the forms.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "access_token=asdf1234&scope=get_form_list&value=NULL&table=checkitout"
   * @returns {array} list of the forms
   */
  getFormList = async ({ projectUrl, accessToken }) => {
    if (!accessToken) {
      throw new Error('Access token is missing.');
    }

    const path = 'pds.php';

    try {
      const res = await fetch(`${projectUrl}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        body: queryString({
          access_token: accessToken,
          scope: 'get_form_list',
        }),
      });

      const json = await res.json();

      if (json.error || json.status !== 'success') {
        throw new Error(json.message || JSON.stringify(json));
      }

      return json.data;
    } catch (e) {
      throw new Error(`API error: getFormList ${e}`);
    }
  }

  /**
   * Gets a form's data by id.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "access_token=abcd1234&scope=get_form_data&value=128&table=checkitout"
   * @returns {array} list of the item definitions
   */
  getFormData = async ({
    projectName,
    projectUrl,
    accessToken,
    id,
  }) => {
    if (!accessToken) {
      throw new Error('Access token is missing.');
    }

    const path = 'pds.php';

    try {
      const res = await fetch(`${projectUrl}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        body: queryString({
          access_token: accessToken,
          scope: 'get_form_data',
          value: id,
          project: projectName,
        }),
      });

      const json = await res.json();
      if (json.error || json.status !== 'success') {
        throw new Error(json.message || JSON.stringify(json));
      }

      return json.data;
    } catch (e) {
      throw new Error(`API error: getFormData ${e}`);
    }
  }

  /**
   * Gets the list of available database projects on a server.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "access_token=abcd1234&scope=get_project_list&value=NULL&table=checkitout"
   * @returns {array} database projects
   */
  async getProjectList({ accessToken }) {
    if (!accessToken) {
      throw new Error('Access token is missing.');
    }

    const path = 'pds.php';

    try {
      const res = await fetch(`${this.apiPrefix}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        body: queryString({
          access_token: accessToken,
          scope: 'get_project_list',
        }),
      });

      const json = await res.json();

      const projects = json.data.map((item) => {
        if (item.project_table === 'lepke_halo') {
          return { ...item, game: 'on' };
        }
        return { ...item };
      });

      if (json.error || json.status !== 'success') {
        throw new Error(json.message || JSON.stringify(json));
      }

      // return json.data;
      return projects;
    } catch (e) {
      throw new Error(`API error: getFormData ${e}`);
    }
  }

  /**
   * Gets the variables associated with the server e.g. login URL for the web login flow.
   * Does not require auth.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "scope=get_project_vars"
   * @returns {array} database projects
   */
  async getProjectVars() {
    const path = 'projects/checkitout/pds.php';

    try {
      const res = await fetch(`${this.apiPrefix}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        body: queryString({
          scope: 'get_project_vars',
        }),
      });

      const json = await res.json();
      if (json.error || json.status !== 'success') {
        throw new Error(json.message || JSON.stringify(json));
      }

      if (!json.data || !json.data.constructor === Array || json.data.length < 1) {
        throw new Error(`API error: 'data' in response JSON should be a 1 length array but was: ${json.data}`);
      }

      const projectVars = json.data[0];

      return {
        projectUrl: projectVars.project_url,
        loginUrl: projectVars.login_url,
        game: projectVars.game,
        publicMapserv: projectVars.public_mapserv,
      };
    } catch (e) {
      throw new Error(`API error: getFormData ${e}`);
    }
  }

  /**
   * Save the new measurement.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "access_token=abcd1234&scope=get_project_list&value=NULL&table=checkitout"
   * @returns {array} database projects
   */
  async dataPush({
    dataTypes,
    accessToken,
    projectUrl,
    formId,
    measurement,
  }) {
    if (!accessToken) {
      throw new Error('Access token is missing.');
    }

    const path = 'pds.php';

    const header = Object.keys(measurement);

    const { id, ...measurementData } = measurement;

    header.forEach((key) => {
      const resType = dataTypes.find(type => type.column === key);
      measurementData[key] = this.typeConverter(resType, key, measurement);
    });

    const query = queryString({
      access_token: accessToken,
      form_id: formId,
      scope: 'put_data',
      header: JSON.stringify(header),
      data: JSON.stringify([measurementData]),
    });

    try {
      const res = await fetch(`${projectUrl}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        body: query,
      });

      const json = await res.json();
      if (json.error || json.status !== 'success') {
        throw json;
      }

      return json.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  typeConverter = (dataTypes, key, measurement) => {
    switch (dataTypes && dataTypes.type) {
      case 'point':
      case 'wkt':
        return `POINT(${measurement[key].longitude} ${measurement[key].latitude})`;
      case 'datetime':
        return moment(measurement[key]).format('YYYY-MM-DD HH:mm:00');
      case 'date':
        return moment(measurement[key]).format('YYYY-MM-DD');
      case 'time':
        return moment(measurement[key]).format('HH:mm:00');
      default:
        return measurement[key];
    }
  }

  /**
   * Save the new measurement.
   *
   * @example curl -v http://openbiomaps.org/projects/checkitout/pds.php -d "access_token=abcd1234&scope=get_project_list&value=NULL&table=checkitout"
   * @returns {array} database projects
   */
  async filePush({
    dataTypes,
    accessToken,
    projectUrl,
    formId,
    measurement,
  }) {
    if (!accessToken) {
      throw new Error('Access token is missing.');
    }

    const path = 'pds.php';

    const header = Object.keys(measurement);

    const imageFields = dataTypes.filter(field => field.type === 'file_id').map(field => field.column);
    const images = imageFields.reduce((acc, curr) =>
      (measurement[curr] ? [...acc, ...measurement[curr]] : acc), []);
    const files = images.map((image, index) => `file${index}`);

    const resMeasurement = { ...measurement };

    header.forEach((key) => {
      const resType = dataTypes.find(type => type.column === key);
      resMeasurement[key] = this.typeConverter(resType, key, measurement);
    });

    const batch = [{
      data: [resMeasurement],
      attached_files: files.join(','),
    }];

    const formData = new FormData();
    formData.append('access_token', accessToken);
    formData.append('form_id', formId);
    formData.append('scope', 'put_data');
    formData.append('header', JSON.stringify(header));
    formData.append('batch', JSON.stringify(batch));
    formData.append('scope', 'put_data');
    images.forEach((file, index) => {
      formData.append(`file${index}`, {
        uri: file.uri,
        type: file.type,
        name: file.fileName,
      });
    });

    try {
      const res = await fetch(`${projectUrl}/${path}`, {
        method: 'POST',
        body: formData,
      });

      const json = await res.json();

      if (json.error || json.status !== 'success') {
        throw json;
      }

      return json.data;
    } catch (error) {
      console.log(error);
      throw error;
    }
  }

  /**
   * Gets a project's trainings.
   *
   * @example curl -F ‘scope=get_trainings’ -F ‘access_token=abcd1234 -F ‘project=dinpi’ http://localhost/biomaps/pds.php
   * @returns {array} list of the item definitions
   */
  getTrainings = async ({ projectName, accessToken, projectUrl }) => {
    if (!accessToken) {
      throw new Error('Access token is missing.');
    }

    const path = 'pds.php';

    try {
      const res = await fetch(`${projectUrl}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        body: queryString({
          access_token: accessToken,
          scope: 'get_trainings',
          project: projectName,
        }),
      });

      const json = await res.json();
      if (json.error || json.status !== 'success') {
        throw new Error(json.message || JSON.stringify(json));
      }

      return json.data;
    } catch (e) {
      console.log(e);
      throw new Error(`API error: getTrainings ${e}`);
    }
  }

  /**
    * Gets the questions and answers for a given training.
    *
    * @example curl -F ‘scope=get_training_questions’ -F ‘access_token=abcd1234 -F ‘project=dinpi’ http://dinpi.openbiomaps.org/biomaps/pds.php -F 'value=1'
    * @returns {array} list of the item definitions
    */
  getTrainingQuestions = async ({
    projectName,
    accessToken,
    trainingId,
    projectUrl,
  }) => {
    if (!accessToken) {
      throw new Error('Access token is missing.');
    }

    const path = 'pds.php';

    try {
      const res = await fetch(`${projectUrl}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        body: queryString({
          access_token: accessToken,
          scope: 'get_training_questions',
          project: projectName,
          value: trainingId,
        }),
      });
      const json = await res.json();
      if (json.error || json.status !== 'success') {
        throw new Error(json.message || JSON.stringify(json));
      }

      return json.data;
    } catch (e) {
      console.log(e);
      throw new Error(`API error: getTrainings ${e}`);
    }
  }

  /**
   * Gets the questions and answers for a given training.
   *
   * @example curl -F ‘scope=training_results’ -F ‘access_token=abcd1234 -F ‘project=dinpi’ http://dinpi.openbiomaps.org/biomaps/pds.php -F 'value=1'
   * @returns {array} list of the results
   */
  trainingResults = async ({ projectName, accessToken, projectUrl }) => {
    if (!accessToken) {
      throw new Error('Access token is missing.');
    }

    const path = 'pds.php';

    try {
      const res = await fetch(`${projectUrl}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        body: queryString({
          access_token: accessToken,
          scope: 'training_results',
          project: projectName,
        }),
      });
      const json = await res.json();
      if (json.error || json.status !== 'success') {
        throw new Error(json.message || JSON.stringify(json));
      }

      return json.data;
    } catch (e) {
      console.log(e);
      throw new Error(`API error: trainingResults ${e}`);
    }
  }

  // curl -F ‘scope=training_toplist’ -F ‘access_token=5ac3...’ -F ‘project=dinpi’ http://localhost/biomaps/pds.php
  trainingToplist = async ({ projectName, accessToken, projectUrl }) => {
    if (!accessToken) {
      throw new Error('Access token is missing.');
    }

    const path = 'pds.php';

    try {
      const res = await fetch(`${projectUrl}/${path}`, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
        },
        body: queryString({
          access_token: accessToken,
          scope: 'training_toplist',
          project: projectName,
        }),
      });

      const json = await res.json();
      if (json.error || json.status !== 'success') {
        throw new Error(json.message || JSON.stringify(json));
      }

      return json.data;
    } catch (e) {
      console.log(e);
      throw new Error(`API error: getTrainings ${e}`);
    }
  }
}
