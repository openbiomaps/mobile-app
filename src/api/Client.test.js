import Client from './Client';

/**
 * @returns {Client} Authenticated mock client.
 */
async function createAuthenticatedClient() {
  const mockAuthResponse = '{"access_token":"731c8c29253c76ae174015fef374bf3358a3b9b2","expires_in":3600,"token_type":"Bearer","scope":"get_form_data get_form_list put_data","refresh_token":"722e45ae964c6c53ae8d77690d6167654f8d3280"}';
  fetch.mockResponseOnce(mockAuthResponse);

  const client = new Client({ apiPrefix: 'http://openbiomaps.org' });

  await client.auth({
    username: 'user',
    password: 'asdf1234',
  });

  return client;
}

test('auth', async () => {
  const mockAuthResponse = '{"access_token":"731c8c29253c76ae174015fef374bf3358a3b9b2","expires_in":3600,"token_type":"Bearer","scope":"get_form_data get_form_list put_data","refresh_token":"722e45ae964c6c53ae8d77690d6167654f8d3280"}';
  fetch.mockResponseOnce(mockAuthResponse);

  const client = new Client({ apiPrefix: 'http://openbiomaps.org' });

  const authObject = await client.auth({
    username: 'user',
    password: 'asdf1234',
  });

  expect(authObject.accessToken).toEqual('731c8c29253c76ae174015fef374bf3358a3b9b2');
  expect(authObject.expiresIn).toEqual(3600);
  expect(authObject.refreshToken).toEqual('722e45ae964c6c53ae8d77690d6167654f8d3280');
  expect(typeof authObject.requestTime).toEqual('number');
});

test('getFormList', async () => {
  const mockFormListResponse = '{"status":"success","data":[{"id":"98","visibility":"K\u00e9t\u00e9lt\u0171 el\u00fct\u00e9sek","form_id":"98","form_name":"K\u00e9t\u00e9lt\u0171 el\u00fct\u00e9sek"},{"id":"92","visibility":"miki","form_id":"92","form_name":"miki"},{"id":"128","visibility":"Szugyi","form_id":"128","form_name":"Szugyi"},{"id":"110","visibility":"tesztformFA","form_id":"110","form_name":"tesztformFA"}]}';

  const client = await createAuthenticatedClient();

  fetch.mockResponseOnce(mockFormListResponse);

  const form = await client.getFormList({ accessToken: '731c8c29253c76ae174015fef374bf3358a3b9b2' });

  expect(form.constructor).toEqual(Array);

  expect(form).toEqual([{
    id: '98', visibility: 'K\u00e9t\u00e9lt\u0171 el\u00fct\u00e9sek', form_id: '98', form_name: 'K\u00e9t\u00e9lt\u0171 el\u00fct\u00e9sek',
  }, {
    id: '92', visibility: 'miki', form_id: '92', form_name: 'miki',
  }, {
    id: '128', visibility: 'Szugyi', form_id: '128', form_name: 'Szugyi',
  }, {
    id: '110', visibility: 'tesztformFA', form_id: '110', form_name: 'tesztformFA',
  }]);
});

test('getFormData', async () => {
  const mockFormDataResponse = '{"status":"success","data":[{"description":null,"default_value":null,"column":"obm_files_id","short_name":"attachement","list":"","control":"nocheck","count":"{}","type":"file_id","genlist":null,"obl":"1","api_params":null,"spatial_limit":null},{"description":"Ez egy komment","default_value":null,"column":"comment","short_name":"comment","list":"","control":"nocheck","count":"{}","type":"text","genlist":null,"obl":"2","api_params":null,"spatial_limit":null},{"description":null,"default_value":null,"column":"datum","short_name":"date","list":"","control":"nocheck","count":"{}","type":"date","genlist":null,"obl":"2","api_params":null,"spatial_limit":null},{"description":null,"default_value":null,"column":"obm_geometry","short_name":"geometry","list":"","control":"nocheck","count":"{}","type":"point","genlist":null,"obl":"2","api_params":null,"spatial_limit":null},{"description":null,"default_value":null,"column":"latitude","short_name":"latitude","list":"","control":"nocheck","count":"{}","type":"numeric","genlist":null,"obl":"2","api_params":null,"spatial_limit":null},{"description":null,"default_value":null,"column":"longitude","short_name":"longitude","list":"","control":"nocheck","count":"{}","type":"numeric","genlist":null,"obl":"2","api_params":null,"spatial_limit":null},{"description":null,"default_value":null,"column":"obm_datum","short_name":"new_datum","list":"","control":"nocheck","count":"{}","type":"datetime","genlist":null,"obl":"2","api_params":null,"spatial_limit":null},{"description":null,"default_value":null,"column":"observer","short_name":"observer","list":"","control":"nocheck","count":"{}","type":"text","genlist":null,"obl":"2","api_params":null,"spatial_limit":null},{"description":null,"default_value":null,"column":"time","short_name":"time","list":"","control":"nocheck","count":"{}","type":"time","genlist":null,"obl":"2","api_params":null,"spatial_limit":null}]}';

  const client = await createAuthenticatedClient();

  fetch.mockResponseOnce(mockFormDataResponse);

  const forms = await client.getFormData({ accessToken: '731c8c29253c76ae174015fef374bf3358a3b9b2', id: 113 });

  expect(forms.constructor).toEqual(Array);

  expect(forms).toEqual([{
    description: null, default_value: null, column: 'obm_files_id', short_name: 'attachement', list: '', control: 'nocheck', count: '{}', type: 'file_id', genlist: null, obl: '1', api_params: null, spatial_limit: null,
  }, {
    description: 'Ez egy komment', default_value: null, column: 'comment', short_name: 'comment', list: '', control: 'nocheck', count: '{}', type: 'text', genlist: null, obl: '2', api_params: null, spatial_limit: null,
  }, {
    description: null, default_value: null, column: 'datum', short_name: 'date', list: '', control: 'nocheck', count: '{}', type: 'date', genlist: null, obl: '2', api_params: null, spatial_limit: null,
  }, {
    description: null, default_value: null, column: 'obm_geometry', short_name: 'geometry', list: '', control: 'nocheck', count: '{}', type: 'point', genlist: null, obl: '2', api_params: null, spatial_limit: null,
  }, {
    description: null, default_value: null, column: 'latitude', short_name: 'latitude', list: '', control: 'nocheck', count: '{}', type: 'numeric', genlist: null, obl: '2', api_params: null, spatial_limit: null,
  }, {
    description: null, default_value: null, column: 'longitude', short_name: 'longitude', list: '', control: 'nocheck', count: '{}', type: 'numeric', genlist: null, obl: '2', api_params: null, spatial_limit: null,
  }, {
    description: null, default_value: null, column: 'obm_datum', short_name: 'new_datum', list: '', control: 'nocheck', count: '{}', type: 'datetime', genlist: null, obl: '2', api_params: null, spatial_limit: null,
  }, {
    description: null, default_value: null, column: 'observer', short_name: 'observer', list: '', control: 'nocheck', count: '{}', type: 'text', genlist: null, obl: '2', api_params: null, spatial_limit: null,
  }, {
    description: null, default_value: null, column: 'time', short_name: 'time', list: '', control: 'nocheck', count: '{}', type: 'time', genlist: null, obl: '2', api_params: null, spatial_limit: null,
  }]);
});

test('getProjectList', async () => {
  // do NOT forget to mask the emails and names before commiting when updating the mock response
  const mockProjetListResponse = '{"status":"success","data":[{"project_table":"harlekin","creation_date":"2013-12-03","Creator":"John Doe","email":"email@email.com","stage":"experimental","doi":null,"running_date":null,"licence":"ODbL","rum":"---","collection_dates":null,"subjects":null},{"project_table":"catax","creation_date":"2015-03-11","Creator":"John Doe","email":"email@email.com","stage":"experimental","doi":null,"running_date":null,"licence":"ODbL","rum":"---","collection_dates":null,"subjects":null},{"project_table":"feher_golya","creation_date":"2016-08-24","Creator":"John Doe","email":"email@email.com","stage":"experimental","doi":null,"running_date":null,"licence":"ODbL","rum":"---","collection_dates":null,"subjects":null},{"project_table":"sciaroidea","creation_date":"2016-09-28","Creator":"John Doe","email":"email@email.com","stage":"experimental","doi":null,"running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"sarand","creation_date":"2014-02-06","Creator":"John Doe","email":"email@email.com","stage":"testing","doi":null,"running_date":null,"licence":"ODbL","rum":"+++","collection_dates":null,"subjects":null},{"project_table":"apple_orchard","creation_date":"2015-08-26","Creator":"John Doe","email":"email@email.com","stage":"stable","doi":null,"running_date":"2015-08-27","licence":"ODbL","rum":"+--","collection_dates":null,"subjects":null},{"project_table":"werew","creation_date":"2016-09-01","Creator":"John Doe","email":"email@email.com","stage":"experimental","doi":null,"running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"transdiptera","creation_date":"2016-11-29","Creator":"John Doe","email":"email@email.com","stage":"stable","doi":"10.18426/OBM.5sskmlll3ip0","running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"plover","creation_date":"2017-04-11","Creator":"John Doe","email":"email@email.com","stage":"testing","doi":null,"running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"eke_botgarden","creation_date":"2015-09-28","Creator":"John Doe","email":"email@email.com","stage":"testing","doi":null,"running_date":null,"licence":"ODbL","rum":"---","collection_dates":null,"subjects":null},{"project_table":"waterbirds","creation_date":"2017-02-09","Creator":"John Doe","email":"email@email.com","stage":"testing","doi":null,"running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"sex_ratio_evolution","creation_date":"2017-01-22","Creator":"John Doe","email":"email@email.com","stage":"stable","doi":null,"running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"checkitout","creation_date":"2016-03-09","Creator":"John Doe","email":"email@email.com","stage":"other","doi":null,"running_date":null,"licence":"ODbL","rum":"---","collection_dates":null,"subjects":null},{"project_table":"openbiomaps_network","creation_date":"2016-04-11","Creator":"John Doe","email":"email@email.com","stage":"stable","doi":null,"running_date":null,"licence":"ODbL","rum":"++-","collection_dates":null,"subjects":null},{"project_table":"szenete","creation_date":"2016-12-26","Creator":"John Doe","email":"email@email.com","stage":"stable","doi":null,"running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"bcd","creation_date":"2017-11-01","Creator":"John Doe","email":"email@email.com","stage":"experimental","doi":null,"running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"balkanherps","creation_date":"2017-04-03","Creator":"John Doe","email":"email@email.com","stage":"stable","doi":null,"running_date":"2017-04-18","licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"panbia","creation_date":"2018-02-05","Creator":"John Doe","email":"email@email.com","stage":"experimental","doi":null,"running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"robinia_atlasz","creation_date":"2018-03-03","Creator":"John Doe","email":"email@email.com","stage":"testing","doi":null,"running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"ring","creation_date":"2017-10-20","Creator":"John Doe","email":"email@email.com","stage":"testing","doi":null,"running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"butterfly_transects_hu","creation_date":"2017-01-27","Creator":"John Doe","email":"email@email.com","stage":"stable","doi":null,"running_date":"2018-03-01","licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"public_nestbox_data","creation_date":"2016-11-26","Creator":"John Doe","email":"email@email.com","stage":"stable","doi":null,"running_date":"2016-11-26","licence":null,"rum":null,"collection_dates":null,"subjects":null},{"project_table":"turmer","creation_date":"2017-10-30","Creator":"John Doe","email":"email@email.com","stage":"testing","doi":null,"running_date":null,"licence":null,"rum":null,"collection_dates":null,"subjects":null}]}';

  const client = await createAuthenticatedClient();

  fetch.mockResponseOnce(mockProjetListResponse);

  const forms = await client.getProjectList({ accessToken: '731c8c29253c76ae174015fef374bf3358a3b9b2', id: 113 });

  expect(forms.constructor).toEqual(Array);

  expect(forms).toEqual([{
    project_table: 'harlekin', creation_date: '2013-12-03', Creator: 'John Doe', email: 'email@email.com', stage: 'experimental', doi: null, running_date: null, licence: 'ODbL', rum: '---', collection_dates: null, subjects: null,
  }, {
    project_table: 'catax', creation_date: '2015-03-11', Creator: 'John Doe', email: 'email@email.com', stage: 'experimental', doi: null, running_date: null, licence: 'ODbL', rum: '---', collection_dates: null, subjects: null,
  }, {
    project_table: 'feher_golya', creation_date: '2016-08-24', Creator: 'John Doe', email: 'email@email.com', stage: 'experimental', doi: null, running_date: null, licence: 'ODbL', rum: '---', collection_dates: null, subjects: null,
  }, {
    project_table: 'sciaroidea', creation_date: '2016-09-28', Creator: 'John Doe', email: 'email@email.com', stage: 'experimental', doi: null, running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'sarand', creation_date: '2014-02-06', Creator: 'John Doe', email: 'email@email.com', stage: 'testing', doi: null, running_date: null, licence: 'ODbL', rum: '+++', collection_dates: null, subjects: null,
  }, {
    project_table: 'apple_orchard', creation_date: '2015-08-26', Creator: 'John Doe', email: 'email@email.com', stage: 'stable', doi: null, running_date: '2015-08-27', licence: 'ODbL', rum: '+--', collection_dates: null, subjects: null,
  }, {
    project_table: 'werew', creation_date: '2016-09-01', Creator: 'John Doe', email: 'email@email.com', stage: 'experimental', doi: null, running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'transdiptera', creation_date: '2016-11-29', Creator: 'John Doe', email: 'email@email.com', stage: 'stable', doi: '10.18426/OBM.5sskmlll3ip0', running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'plover', creation_date: '2017-04-11', Creator: 'John Doe', email: 'email@email.com', stage: 'testing', doi: null, running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'eke_botgarden', creation_date: '2015-09-28', Creator: 'John Doe', email: 'email@email.com', stage: 'testing', doi: null, running_date: null, licence: 'ODbL', rum: '---', collection_dates: null, subjects: null,
  }, {
    project_table: 'waterbirds', creation_date: '2017-02-09', Creator: 'John Doe', email: 'email@email.com', stage: 'testing', doi: null, running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'sex_ratio_evolution', creation_date: '2017-01-22', Creator: 'John Doe', email: 'email@email.com', stage: 'stable', doi: null, running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'checkitout', creation_date: '2016-03-09', Creator: 'John Doe', email: 'email@email.com', stage: 'other', doi: null, running_date: null, licence: 'ODbL', rum: '---', collection_dates: null, subjects: null,
  }, {
    project_table: 'openbiomaps_network', creation_date: '2016-04-11', Creator: 'John Doe', email: 'email@email.com', stage: 'stable', doi: null, running_date: null, licence: 'ODbL', rum: '++-', collection_dates: null, subjects: null,
  }, {
    project_table: 'szenete', creation_date: '2016-12-26', Creator: 'John Doe', email: 'email@email.com', stage: 'stable', doi: null, running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'bcd', creation_date: '2017-11-01', Creator: 'John Doe', email: 'email@email.com', stage: 'experimental', doi: null, running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'balkanherps', creation_date: '2017-04-03', Creator: 'John Doe', email: 'email@email.com', stage: 'stable', doi: null, running_date: '2017-04-18', licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'panbia', creation_date: '2018-02-05', Creator: 'John Doe', email: 'email@email.com', stage: 'experimental', doi: null, running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'robinia_atlasz', creation_date: '2018-03-03', Creator: 'John Doe', email: 'email@email.com', stage: 'testing', doi: null, running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'ring', creation_date: '2017-10-20', Creator: 'John Doe', email: 'email@email.com', stage: 'testing', doi: null, running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'butterfly_transects_hu', creation_date: '2017-01-27', Creator: 'John Doe', email: 'email@email.com', stage: 'stable', doi: null, running_date: '2018-03-01', licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'public_nestbox_data', creation_date: '2016-11-26', Creator: 'John Doe', email: 'email@email.com', stage: 'stable', doi: null, running_date: '2016-11-26', licence: null, rum: null, collection_dates: null, subjects: null,
  }, {
    project_table: 'turmer', creation_date: '2017-10-30', Creator: 'John Doe', email: 'email@email.com', stage: 'testing', doi: null, running_date: null, licence: null, rum: null, collection_dates: null, subjects: null,
  }]);
});

test('getProjectVars', async () => {
  const mockFormListResponse = '{"status":"success","data":[{"project_url":"http://openbiomaps.org/projects/checkitout/","login_url":"http://openbiomaps.org/projects/checkitout/?login","game":"off","public_mapserv":"openbiomaps.org/projects/checkitout/public/proxy.php"}]}';

  const client = await createAuthenticatedClient();

  fetch.mockResponseOnce(mockFormListResponse);

  const projectVars = await client.getProjectVars();

  expect(projectVars).toEqual({
    projectUrl: 'http://openbiomaps.org/projects/checkitout/',
    loginUrl: 'http://openbiomaps.org/projects/checkitout/?login',
    game: 'off',
    publicMapserv: 'openbiomaps.org/projects/checkitout/public/proxy.php',
  });
});
