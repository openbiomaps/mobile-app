import { TOPLIST_FETCH_SUCCESS } from './types';
import { performInDatabaseList } from '.';
import client from '../config/apiClient';

export const loadToplist = ({ accessToken, database }) => async (dispatch) => {
  const { id, name: projectName, projectUrl } = database;

  try {
    const res = await client.trainingToplist({ accessToken, projectName, projectUrl });

    const toplist = Object.entries(res).map(([name, value]) => ({
      name,
      score: Number(value.max).toFixed(3),
    }));
    toplist.sort((a, b) => b.score - a.score);
    dispatch(performInDatabaseList(id, {
      type: TOPLIST_FETCH_SUCCESS,
      payload: { toplist },
    }));
  } catch (e) {
    console.log(e);
  }
};
