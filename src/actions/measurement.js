import {
  ADD_TO_MEASUREMENT_LIST,
  REMOVE_FROM_MEASUREMENT_LIST,
  PERFORM_IN_MEASUREMENT_LIST,
  SELECT_MEASUREMENT,
  SET_SYNC_STATUS,
  SYNC_DATA_START,
  MEASUREMENT_FETCH_START,
  MEASUREMENT_FETCH_ERROR,
  FORM_DATA_FETCH_SUCCESS,
  SET_STICKINESSES,
  UPDATE_MEASUREMENT,
  MEASUREMENT_EDIT_START,
} from './types';
import { performInObservationList, sliceMeasurementList } from '.';
import client from '../config/apiClient';
import { parseWkt, isRefreshTokenExpired } from '../utils';

const fetchStart = () => ({
  type: MEASUREMENT_FETCH_START,
});

const fetchError = error => ({
  type: MEASUREMENT_FETCH_ERROR,
  error,
});

export const loadFormData = ({
  projectName,
  projectUrl,
  accessToken,
  databaseListId,
  observationListId,
}) =>
  async (dispatch) => {
    dispatch(fetchStart());

    try {
      const result = await client.getFormData({
        projectName,
        projectUrl,
        accessToken,
        id: observationListId,
      });

      const formData = result.map(({
        api_params,
        default_value,
        short_name,
        spatial_limit,
        permanent_sample_plots,
        ...restParams
      }) => ({
        apiParams: api_params,
        defaultValue: default_value,
        shortName: short_name,
        spatialLimit: spatial_limit,
        permanentSamplePlots: permanent_sample_plots &&
          permanent_sample_plots.filter(plot => plot.geometry).map(({ name, geometry }, index) => ({
            id: index,
            name,
            ...parseWkt(geometry),
          })),
        ...restParams,
      }));

      const locations = formData.filter(field =>
        field.permanentSamplePlots).reduce((acc, curr) =>
        [...acc, ...(curr.permanentSamplePlots)], []);

      dispatch(performInObservationList(databaseListId, observationListId, {
        type: FORM_DATA_FETCH_SUCCESS,
        payload: {
          dataTypes: formData,
          permanentSamplePlots: locations,
        },
      }));
    } catch (error) {
      console.log(error);
      dispatch(fetchError(error));
    }
  };


/**
   * Add measurement object to the list
   * @param {number} databaseListId - id of the database which will contains the observation
   * @param {number} observationListId - id of the observation which will contains the measurement
   * @param {object} payload - the properties of the new observation
   */
export const createMeasurement =
(databaseListId, observationListId, payload = {}) =>
  dispatch =>
    dispatch(performInObservationList(databaseListId, observationListId, {
      type: ADD_TO_MEASUREMENT_LIST,
      payload,
    }));

  /**
   * Remove the item from the list
   * @param {number} databaseListId - id of the database which contains the observation
   * @param {number} observationId - id of the deleted observation
   */
export const removeFromMeasurementList =
(databaseListId, observationListId, measurementId) =>
  dispatch =>
    dispatch(performInObservationList(databaseListId, observationListId, {
      type: REMOVE_FROM_MEASUREMENT_LIST,
      id: measurementId,
    }));

  /**
   * Perform an action in the observation list
   * @param {number} databaseListId - id of the database which contains the observation
   * @param {number} observationId - id of the target observation
   * @param {object} action - the action what we want to perform in the observation list
   */
export const performInMeasurementList =
  (databaseListId, observationListId, measurementId, action) =>
    (dispatch) => {
      dispatch(performInObservationList(databaseListId, observationListId, {
        type: PERFORM_IN_MEASUREMENT_LIST,
        id: measurementId,
        action,
      }));
    };

export const updateMeasurement =
  (databaseListId, observationListId, measurementId, payload) =>
    (dispatch) => {
      dispatch(performInMeasurementList(databaseListId, observationListId, measurementId, {
        type: UPDATE_MEASUREMENT,
        payload,
      }));
    };

export const setStickinesses =
(databaseListId, observationListId, stickinesses) => dispatch =>
  dispatch(performInObservationList(databaseListId, observationListId, {
    type: SET_STICKINESSES,
    payload: stickinesses,
  }));

export const selectMeasurement =
(databaseListId, observationListId, measurementId) => dispatch =>
  dispatch(performInObservationList(databaseListId, observationListId, {
    type: SELECT_MEASUREMENT,
    id: measurementId,
  }));

export const syncDataStart =
(databaseListId, observationListId, measurementId) =>
  dispatch =>
    dispatch(performInMeasurementList(
      databaseListId,
      observationListId,
      measurementId, {
        type: SYNC_DATA_START,
      },
    ));

export const syncDataSuccess =
(databaseListId, observationListId, measurementId, measurementErrors) =>
  dispatch =>
    dispatch(performInMeasurementList(
      databaseListId,
      observationListId,
      measurementId, {
        type: SET_SYNC_STATUS,
        payload: {
          errors: measurementErrors,
          status: true,
        },
      },
    ));

/**
 * Try to sync one single measurement point
 */
export const syncMeasurement = ({
  dataTypes,
  database,
  observation,
  measurement,
}) => async (dispatch, getState) => {
  const { network: { status }, user, hasTokenListener } = getState();
  const { accessToken } = user;
  if (status === 'wifi' && !isRefreshTokenExpired(user) && hasTokenListener && !measurement.isEditing) {
    dispatch(syncDataStart(database.id, observation.id, measurement.id));
    const { id, ...measurementData } = measurement;

    const imageFields = dataTypes.filter(field => field.type === 'file_id').map(field => field.column);
    const imageCount = imageFields.reduce((acc, curr) =>
      acc + (measurement[curr] ? measurement[curr].length : 0), 0);

    let errors = null;
    try {
      if (imageCount) {
        await client.filePush({
          dataTypes,
          accessToken,
          projectUrl: database.projectUrl,
          formId: observation.id,
          measurement: measurementData,
        });
      } else {
        await client.dataPush({
          dataTypes,
          accessToken,
          projectUrl: database.projectUrl,
          formId: observation.id,
          measurement: measurementData,
        });
      }
    } catch (error) {
      try {
        const message = JSON.parse(error.message);
        Object.entries(message).forEach(([key, value]) => {
          message[key] = value['1'];
        });
        errors = message;
      } catch (err) {
        errors = { errorMessage: error.message };
      }
    }
    dispatch(syncDataSuccess(
      database.id,
      observation.id,
      measurement.id,
      errors,
    ));

    // return true value if the uploaded data is correct
    return !errors;
  }
  // return true if we don't have wifi connection or the accessToken has expired
  return true;
};

export const syncMeasurements = () => async (dispatch, getState) => {
  const { databases } = getState();
  // sync data
  databases.forEach((database) => {
    if (database.observations) {
      database.observations.forEach((observation) => {
        if (observation.measurements) {
          const dataTypes = observation.form ? observation.form.dataTypes : null;
          observation.measurements.forEach(async (measurement) => {
            if (!measurement.isSynced && !measurement.isEditing) {
              dispatch(syncMeasurement({
                dataTypes,
                database,
                observation,
                measurement: measurement.data,
              }));
            }
          });
          dispatch(performInObservationList(
            database.id,
            observation.id,
            sliceMeasurementList(observation),
          ));
        }
      });
    }
  });
};

export const editMeasurement = (databaseListId, observationListId, measurementId) =>
  (dispatch) => {
    dispatch(performInMeasurementList(databaseListId, observationListId, measurementId, {
      type: MEASUREMENT_EDIT_START,
    }));
  };
