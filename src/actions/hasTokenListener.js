import { START_TOKEN_LISTENER, STOP_TOKEN_LISTENER } from './types';

export const startTokenListener = () => ({
  type: START_TOKEN_LISTENER,
});

export const stopTokenListener = () => ({
  type: STOP_TOKEN_LISTENER,
});
