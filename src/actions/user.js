import moment from 'moment';
import { FETCH_START, FETCH_ERROR, LOGIN_SUCCESS, LOGOUT_SUCCESS, CLOSE_FIRST_USE_HINT, PURGE_DATA } from './types';
import { navigateTo, syncMeasurements, tokenRefresh as refresh, startTokenListener, stopTokenListener } from './index';
import client from '../config/apiClient';
import I18n from '../i18n';
import { isRefreshTokenExpired } from '../utils';
import NavigationService from '../NavigationService';

const fetchStart = () => ({
  type: FETCH_START,
});

const fetchError = error => ({
  type: FETCH_ERROR,
  error,
});

export const refreshTokenListener = () => (dispatch, getState) => {
  const {
    network,
    user: {
      requestTime,
      expiresIn,
      refreshToken,
    },
  } = getState();
  if (!refreshToken) {
    dispatch(stopTokenListener());
    return;
  }

  const netStatus = network.status;
  const hasNet = netStatus && netStatus !== 'none' && netStatus !== 'unknown';

  if (!hasNet) {
    dispatch(stopTokenListener());
    return;
  }

  const diff = moment(moment()).diff(requestTime);

  if (isRefreshTokenExpired({ requestTime, expiresIn })) {
    dispatch(refresh({ refreshToken }));
  } else {
    setTimeout(() => {
      dispatch(refreshTokenListener());
    }, expiresIn - diff);
  }
};

export const logout = () => (dispatch) => {
  dispatch({ type: LOGOUT_SUCCESS });
  NavigationService.dispatch(navigateTo('Login'));
};

export const startRefreshTokenListener = () => (dispatch, getState) => {
  const { hasTokenListener } = getState();
  if (hasTokenListener) { return; }
  dispatch(startTokenListener());
  dispatch(refreshTokenListener());
};

export const tokenRefresh = ({ refreshToken }) => async (dispatch, getState) => {
  dispatch(fetchStart());
  try {
    const authResult = await client.refresh({ refreshToken });

    dispatch({
      type: LOGIN_SUCCESS,
      payload: {
        ...authResult,
      },
    });

    const { network: { status } } = getState();
    if (status === 'wifi') {
      dispatch(syncMeasurements());
    }

    dispatch(refreshTokenListener());
  } catch (error) {
    dispatch(stopTokenListener());
    dispatch(logout());
  }
};

export const login = ({ username, password }) => async (dispatch, getState) => {
  dispatch(fetchStart());
  try {
    const authResult = await client.auth({ username, password });

    dispatch({
      type: LOGIN_SUCCESS,
      payload: {
        username,
        // password,
        ...authResult,
      },
    });

    const { network: { status } } = getState();
    if (status === 'wifi') {
      dispatch(syncMeasurements());
    }

    dispatch(startRefreshTokenListener());
  } catch (error) {
    const message = error.error === 'invalid_grant' ? I18n.t('invalid_username_password') : I18n.t('login_error');
    dispatch(fetchError(message));
  }
};

export const closeFirstUseHint = () => ({
  type: CLOSE_FIRST_USE_HINT,
});

export const purgeData = () => ({
  type: PURGE_DATA,
});
