import {
  ADD_TO_OBSERVATION_LIST,
  REMOVE_FROM_OBSERVATION_LIST,
  PERFORM_IN_OBSERVATION_LIST,
  OBSERVATION_FETCH_START,
  OBSERVATION_FETCH_ERROR,
  OBSERVATION_FETCH_SUCCESS,
  SELECT_OBSERVATION,
  UPDATE_OBSERVATION,
} from './types';
import { performInDatabaseList } from '.';
import { loadGameContents } from './game';
import client from '../config/apiClient';


const fetchStart = () => ({
  type: OBSERVATION_FETCH_START,
});

const fetchError = error => ({
  type: OBSERVATION_FETCH_ERROR,
  error,
});

export const loadObservations = (database, accessToken) =>
  async (dispatch) => {
    dispatch(fetchStart());

    try {
      const result = await client.getFormList({ projectUrl: database.projectUrl, accessToken });

      const observations = result.map(({ form_id, form_name }) => ({
        id: form_id,
        name: form_name,
      }));

      dispatch(performInDatabaseList(database.id, {
        type: OBSERVATION_FETCH_SUCCESS,
        payload: observations,
      }));


      if (database.game === 'on') {
        dispatch(loadGameContents(database, accessToken));
      }
    } catch (error) {
      console.log(error);
      dispatch(fetchError(error));
    }
  };

/**
 * Add empty observation object to the list
 * @param {number} databaseListId - id of the database which will contains the observation
 * @param {object} payload - the properties of the new observation
 */
export const addToObservationList = (databaseListId, payload = {}) =>
  dispatch =>
    dispatch(performInDatabaseList(databaseListId, {
      type: ADD_TO_OBSERVATION_LIST,
      payload,
    }));

/**
 * Remove the item from the list
 * @param {number} databaseListId - id of the database which contains the observation
 * @param {number} observationId - id of the deleted observation
 */
export const removeFromObservationList = (databaseListId, observationId) =>
  dispatch =>
    dispatch(performInDatabaseList(databaseListId, {
      type: REMOVE_FROM_OBSERVATION_LIST,
      id: observationId,
    }));

/**
 * Perform an action in the observation list
 * @param {number} databaseListId - id of the database which contains the observation
 * @param {number} observationId - id of the target observation
 * @param {object} action - the action what we want to perform in the observation list
 */
export const performInObservationList =
(databaseListId, observationId, action) =>
  dispatch =>
    dispatch(performInDatabaseList(databaseListId, {
      type: PERFORM_IN_OBSERVATION_LIST,
      id: observationId,
      action,
    }));

export const selectObservation = (databaseListId, observationId) => dispatch =>
  dispatch(performInDatabaseList(databaseListId, {
    type: SELECT_OBSERVATION,
    id: observationId,
    date: new Date(),
  }));

export const sliceMeasurementList =
  (observation, maxMeasurementCount = 100) => {
    const { measurements, ...newObservation } = observation;
    if (measurements.length >= maxMeasurementCount) {
      const isDeletable = measurement => measurement.isSynced && !measurement.errors;
      const lengthDiff = measurements.length - maxMeasurementCount;
      const deletableMeasurements = measurements.filter(isDeletable);
      measurements.sort((a, b) => {
        if (isDeletable(a) && !isDeletable(b)) {
          return 1;
        }
        if (isDeletable(b) && !isDeletable(a)) {
          return -1;
        }
        return b.id - a.id;
      });
      const toBeDeletedCount = Math.max(Math.min(lengthDiff, deletableMeasurements.length), 0);
      measurements.splice(measurements.length - toBeDeletedCount);
    }
    newObservation.measurements = measurements;
    return {
      type: UPDATE_OBSERVATION,
      payload: newObservation,
    };
  };
