import { SET_NETWORK_STATUS, SYNC_START, SYNC_ERROR, SYNC_SUCCESS } from './types';

export const changeNetworkStatus = networkType => ({
  type: SET_NETWORK_STATUS,
  payload: networkType,
});

export const syncDate = () => (dispatch) => {
  dispatch({
    type: SYNC_START,
  });

  try {
    /*
    const response = await fetch(...);
  */
    dispatch({
      type: SYNC_SUCCESS,
    });
  } catch (error) {
    dispatch({
      type: SYNC_ERROR,
    });
  }
};

