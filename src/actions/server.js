import { SELECT_SERVER } from './types';

export const selectServer = payload => ({
  type: SELECT_SERVER,
  payload,
});
