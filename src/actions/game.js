import { GAME_FETCH_SUCCESS, QUIZ_PASSED } from './types';
import { performInObservationList } from '.';
import client from '../config/apiClient';

const loadTrainingQuestions = async data =>
  (await client.getTrainingQuestions(data)).map(({
    training_id,
    answers,
    ...data
  }) => ({
    trainingId: training_id,
    answers: answers.map(({ Answer, isRight }) => ({ text: Answer, isRight: isRight === 'true' })),
    ...data,
  }));

export const loadGameContents = (database, accessToken) => async (dispatch) => {
  try {
    const res = await client.getTrainings({
      projectName: database.name,
      accessToken,
      projectUrl: database.projectUrl,
    });

    const trainings = res.map(({
      enabled,
      task_description,
      project_table,
      form_id,
      ...data
    }) => ({
      isEnabled: enabled === 't',
      taskDescription: task_description,
      projectTable: project_table,
      formId: form_id,
      ...data,
    }));

    const trainingResults = await client.trainingResults({
      projectName: database.name,
      accessToken,
      projectUrl: database.projectUrl,
    });

    trainings.forEach(async (training) => {
      try {
        const questions = await loadTrainingQuestions({
          projectName: database.name,
          accessToken,
          trainingId: training.id,
          projectUrl: database.projectUrl,
        });

        dispatch(performInObservationList(database.id, training.formId, {
          type: GAME_FETCH_SUCCESS,
          payload: {
            ...training,
            questions,
            // possible values are:
            // 1 - validated by backend, 0 - waiting for validation, -1 not completed yet
            isValidatedByBackend: trainingResults[training.formId] === 1,
          },
        }));
      } catch (e) {
        console.log(e);
      }
    });
  } catch (e) {
    console.log(e);
  }
};

export const quizPassed = (databaseId, observationId) => (dispatch) => {
  dispatch(performInObservationList(databaseId, observationId, {
    type: QUIZ_PASSED,
  }));
};
