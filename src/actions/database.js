import {
  ADD_TO_DATABASE_LIST,
  REMOVE_FROM_DATABASE_LIST,
  PERFORM_IN_DATABASE_LIST,
  SELECT_DATABASE,
  DATABASE_FETCH_START,
  DATABASE_FETCH_ERROR,
  DATABASE_FETCH_SUCCESS,
} from './types';
import client from '../config/apiClient';


const fetchStart = () => ({
  type: DATABASE_FETCH_START,
});

const fetchError = error => ({
  type: DATABASE_FETCH_ERROR,
  error,
});

export const loadDatabases = accessToken => async (dispatch) => {
  dispatch(fetchStart());

  try {
    const result = await client.getProjectList({ accessToken });

    const databases = result.map(({
      project_table,
      stage,
      game,
      project_url,
    }, index) => ({
      id: index,
      name: project_table,
      description: stage,
      game,
      settingsUrl: `${project_url}?login`,
      projectUrl: project_url,
    }));


    dispatch({
      type: DATABASE_FETCH_SUCCESS,
      payload: databases,
    });
  } catch (error) {
    dispatch(fetchError(error));
  }
};

/**
 * Add empty database object to the list
 * @param {object} payload - the properties of the new database
 */
export const addToDatabaseList = (payload = {}) => ({
  type: ADD_TO_DATABASE_LIST,
  payload,
});

/**
 * Remove item from the list
 * @param {number} databaseId - id of the deleted database
 */
export const removeFromDatabaseList = databaseId => ({
  type: REMOVE_FROM_DATABASE_LIST,
  id: databaseId,
});


/**
 * Perform an action in the database list
 * @param {number} databaseId - id of the target database
 * @param {object} action - the action what we want to perform in the database list
 */
export const performInDatabaseList = (databaseId, action) => ({
  type: PERFORM_IN_DATABASE_LIST,
  id: databaseId,
  action,
});

export const selectDatabase = databaseId => ({
  type: SELECT_DATABASE,
  id: databaseId,
  date: new Date(),
});

