import {
  FETCH_START,
  FETCH_ERROR,
  SET_SYNC_STATUS,
  SYNC_DATA_START,
  ADD_TO_MEASUREMENT_LIST,
  UPDATE_MEASUREMENT,
  MEASUREMENT_EDIT_START,
} from '../../../actions/types';

const initialState = {
  isSynced: false,
  isEditing: false,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case ADD_TO_MEASUREMENT_LIST: {
      const id = action.payload ? action.payload.id : 0;
      return {
        ...state,
        id,
        data: { ...action.payload },
        isEditing: false,
      };
    }
    case FETCH_START:
      return {
        ...state,
        error: null,
        fetching: true,
      };
    case FETCH_ERROR:
      return {
        ...state,
        error: action.error,
        fetching: false,
      };
    case SYNC_DATA_START: {
      return {
        ...state,
        syncing: true,
        isSynced: false,
      };
    }
    case SET_SYNC_STATUS: {
      const { payload: { status, errors } } = action;
      return {
        ...state,
        errors,
        syncing: false,
        isSynced: status,
      };
    }
    case UPDATE_MEASUREMENT: {
      const { payload } = action;
      return {
        ...state,
        syncing: false,
        isSynced: false,
        data: { ...payload },
        errors: null,
        isEditing: false,
      };
    }
    case MEASUREMENT_EDIT_START: {
      return {
        ...state,
        isEditing: true,
      };
    }
    default:
      return state;
  }
};
