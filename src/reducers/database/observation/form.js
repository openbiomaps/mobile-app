import {
  FETCH_START,
  FETCH_ERROR,
  SET_STICKINESSES,
  FORM_DATA_FETCH_SUCCESS,
} from '../../../actions/types';

const initialState = {
  dataTypes: [],
  error: null,
  fetching: false,
  permanentSamplePlots: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_START:
      return {
        ...state,
        error: null,
        fetching: true,
      };
    case FETCH_ERROR:
      return {
        ...state,
        error: action.error,
        fetching: false,
      };
    case FORM_DATA_FETCH_SUCCESS:
      return {
        ...state,
        ...action.payload,
      };
    case SET_STICKINESSES:
      return {
        ...state,
        stickinesses: { ...action.payload },
      };
    default:
      return state;
  }
};

