import { combineReducers } from 'redux';
import { MEASUREMENT_LIST_TYPE, OBSERVATION_LIST_TYPE } from '../../../actions/types';

import form from './form';
import measurement from './measurement';
import list from '../../list';
import game from './game';
import { stateMapper } from '../../itemProperty';

const measurementList = list(measurement, MEASUREMENT_LIST_TYPE);

const initialState = {
  id: null,
  name: null,
  isSelected: false,
  lastSelected: null,
};

const observationReducer = combineReducers({
  ...stateMapper(initialState, OBSERVATION_LIST_TYPE),
  form,
  game,
  measurements: measurementList,
});

export default observationReducer;
