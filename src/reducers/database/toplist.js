import { TOPLIST_FETCH_SUCCESS } from '../../actions/types';

export default (state = [], action) => {
  switch (action.type) {
    case TOPLIST_FETCH_SUCCESS:
      return [...action.payload.toplist];
    default:
      return state;
  }
};

