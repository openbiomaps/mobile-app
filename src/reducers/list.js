import { PURGE_DATA } from '../actions/types';

export default function list(reducer, listType) {
  return (state = [], action) => {
    const {
      id,
      action: innerAction,
    } = action;

    switch (action.type) {
      case `${listType}_FETCH_SUCCESS`: {
        const list = action.payload.map((listItem) => {
          const orig = state.find(item => item.id === listItem.id);
          if (orig) {
            return {
              ...orig,
              ...listItem,
            };
          }
          return { ...listItem };
        });
        return [
          ...list,
        ];
      }
      case `ADD_TO_${listType}_LIST`:
        return [
          ...state,
          reducer(undefined, action),
        ];
      case `REMOVE_FROM_${listType}_LIST`: {
        const newArray = state.filter(stateItem => stateItem.id !== id);
        return [
          ...newArray,
        ];
      }
      case `SELECT_${listType}`: {
        const { id, date } = action;
        const newState = state.map((item) => {
          if (item.id === id) {
            return {
              ...item,
              isSelected: true,
              lastSelected: date,
            };
          }
          return { ...item, isSelected: false };
        });
        return [
          ...newState,
        ];
      }
      case `UPDATE_${listType}`: {
        const { id, payload } = action;
        const newState = state.map((item) => {
          if (item.id === id) {
            return { ...payload };
          }
          return item;
        });
        return newState;
      }
      case `PERFORM_IN_${listType}_LIST`: {
        const newArray = state.map(stateItem => ((stateItem.id === id) ?
          reducer(stateItem, innerAction) :
          stateItem));

        return [
          ...newArray,
        ];
      }
      case PURGE_DATA: {
        return [];
      }
      default:
        return state;
    }
  };
}
