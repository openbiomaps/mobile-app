import {
  FETCH_START,
  FETCH_ERROR,
  LOGIN_SUCCESS,
  LOGOUT_SUCCESS,
  CLOSE_FIRST_USE_HINT,
  PURGE_DATA,
} from '../actions/types';

const initialState = {
  error: null,
  fetching: false,
  accessToken: null,
  refreshToken: null,
  isFirstUse: true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case FETCH_START:
      return {
        ...state,
        error: null,
        fetching: true,
      };
    case FETCH_ERROR:
      return {
        ...state,
        error: action.error,
        fetching: false,
      };
    case LOGIN_SUCCESS:
      return {
        ...state,
        error: null,
        fetching: false,
        ...action.payload,
      };
    case LOGOUT_SUCCESS:
      return {
        ...state,
        accessToken: null,
        refreshToken: null,
      };
    case CLOSE_FIRST_USE_HINT:
      return {
        ...state,
        isFirstUse: false,
      };
    case PURGE_DATA:
      return {
        ...initialState,
      };
    default:
      return state;
  }
};
