import { persistCombineReducers } from 'redux-persist';
import storage from 'redux-persist/es/storage';
import { DATABASE_LIST_TYPE } from '../actions/types';

import list from './list';
import user from './user';
import server from './server';
import network from './network';
import database from './database';
import hasTokenListener from './hasTokenListener';

const config = {
  key: 'root',
  debug: process.env.NODE_ENV === 'development',
  storage,
  whitelist: ['user', 'server', 'databases'],
};

const databaseList = list(database, DATABASE_LIST_TYPE);

export default persistCombineReducers(config, {
  user,
  server,
  databases: databaseList,
  network,
  hasTokenListener,
});
