import observationReducer from '../database/observation';
import databaseReducer from '../database';
import list from '../list';
import {
  ADD_TO_OBSERVATION_LIST,
  DATABASE_LIST_TYPE,
  PERFORM_IN_DATABASE_LIST,
} from '../../actions/types';

const databaseList = list(databaseReducer, DATABASE_LIST_TYPE);

describe('observation reducer', () => {
  it('should return the initial state', () => {
    expect(observationReducer(undefined, {})).toEqual({
      form: {
        dataTypes: [],
        error: null,
        fetching: false,
        permanentSamplePlots: [],
      },
      game: null,
      id: null,
      isSelected: false,
      lastSelected: null,
      measurements: [],
      name: null,
    });
  });

  it('should handle ADD_TO_OBSERVATION_LIST', () => {
    expect(observationReducer(undefined, {
      type: ADD_TO_OBSERVATION_LIST,
      payload: { id: 0, name: 'observation 1' },
    })).toEqual({
      form: {
        dataTypes: [],
        error: null,
        fetching: false,
        permanentSamplePlots: [],
      },
      game: null,
      id: 0,
      isSelected: false,
      lastSelected: null,
      measurements: [],
      name: 'observation 1',
    });
  });

  it('should handle ADD_TO_OBSERVATION_LIST if we have a default state', () => {
    const state = [{
      id: 0,
      isSelected: false,
      name: 'database 1',
      observations: [],
      settingsUrl: 'http://test-case.url',
      description: 'test',
      game: 'off',
      lastSelected: null,
      toplist: [],
    }];

    const action = {
      type: PERFORM_IN_DATABASE_LIST,
      id: 0,
      action: {
        type: ADD_TO_OBSERVATION_LIST,
        payload: { id: 0, name: 'observation 0' },
      },
    };

    expect(databaseList(state, action)).toEqual([{
      id: 0,
      isSelected: false,
      name: 'database 1',
      settingsUrl: 'http://test-case.url',
      description: 'test',
      game: 'off',
      lastSelected: null,
      toplist: [],
      projectUrl: null,
      observations: [{
        form: {
          dataTypes: [],
          error: null,
          fetching: false,
          permanentSamplePlots: [],
        },
        game: null,
        id: 0,
        isSelected: false,
        lastSelected: null,
        measurements: [],
        name: 'observation 0',
      }],
    }]);
  });
});
