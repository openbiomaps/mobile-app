import { SET_NETWORK_STATUS, SYNC_START, SYNC_ERROR, SYNC_SUCCESS } from '../actions/types';

const VALID_NET_STATUSES = [
  'none',
  'wifi',
  'cellular',
  'unknown',
  // Android only
  'bluetooth',
  'ethernet',
  'wimax',
];

const initialState = {
  fetching: false,
  error: null,
  status: null,
  synced: true,
};

export default (state = initialState, action) => {
  switch (action.type) {
    case SET_NETWORK_STATUS: {
      const { payload } = action;
      const status = VALID_NET_STATUSES.indexOf(payload) !== -1 ? payload : 'unknown';
      return {
        ...state,
        status,
      };
    }
    case SYNC_START:
      return {
        ...state,
        fetching: true,
        error: null,
      };
    case SYNC_ERROR:
      return {
        ...state,
        synced: false,
        fetching: false,
        error: action.error,
      };
    case SYNC_SUCCESS:
      return {
        ...state,
        synced: true,
        fetching: false,
        error: null,
      };
    default:
      return state;
  }
};
