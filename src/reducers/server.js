import { SELECT_SERVER, PURGE_DATA } from '../actions/types';

const initialState = null;

export default (state = initialState, action) => {
  switch (action.type) {
    case SELECT_SERVER: {
      return action.payload;
    }
    case PURGE_DATA:
      return null;
    default:
      return state;
  }
};
