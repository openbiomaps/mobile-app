import { START_TOKEN_LISTENER, STOP_TOKEN_LISTENER } from '../actions/types';

export default (state = false, action) => {
  switch (action.type) {
    case START_TOKEN_LISTENER:
      return true;
    case STOP_TOKEN_LISTENER:
      return false;
    default:
      return state;
  }
};
