import { applyMiddleware, createStore } from 'redux';
import { persistStore } from 'redux-persist';
import thunk from 'redux-thunk';
import { composeWithDevTools } from 'redux-devtools-extension';

import reducers from '../reducers';

const middleware = [thunk];

export default () => {
  const store = createStore(
    reducers,
    composeWithDevTools(applyMiddleware(...middleware)),
  );

  const persistor = persistStore(store);
  return { persistor, store };
};
