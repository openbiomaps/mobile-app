import React, { Component } from 'react';
import { BackHandler, NetInfo, View } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import Routes from './Routes';
import { changeNetworkStatus, syncMeasurements, startRefreshTokenListener } from './actions';
import { isRefreshTokenExpired } from './utils';
import NavigationService from './NavigationService';

class AppNavigation extends Component {
  componentDidMount = async () => {
    const netStatus = await NetInfo.getConnectionInfo();
    this.handleNetworkChange(netStatus);
    NetInfo.addEventListener('connectionChange', this.handleNetworkChange);
    BackHandler.addEventListener('hardwareBackPress', this.onBackPress);
  };

  componentWillUnmount() {
    NetInfo.removeEventListener('connectionChange', this.handleNetworkChange);
    BackHandler.removeEventListener('hardwareBackPress', this.onBackPress);
  }

  onBackPress = () => {};

  handleNetworkChange = ({ type }) => {
    this.props.changeNetworkStatus(type);
    const { user, hasTokenListener } = this.props;
    if (type === 'wifi' && !isRefreshTokenExpired(user) && hasTokenListener) {
      this.props.syncMeasurements();
    } else if (type && type !== 'unknown' && type !== 'none') {
      this.props.startRefreshTokenListener();
    }
  };

  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#fff' }} >
        <Routes ref={NavigationService.setTopLevelNavigator} />
      </View>
    );
  }
}

AppNavigation.propTypes = {
  changeNetworkStatus: PropTypes.func.isRequired,
  syncMeasurements: PropTypes.func.isRequired,
  user: PropTypes.shape({
    expiresIn: PropTypes.number.isRequired,
    requestTime: PropTypes.number.isRequired,
  }).isRequired,
  startRefreshTokenListener: PropTypes.func.isRequired,
  hasTokenListener: PropTypes.bool.isRequired,
};

const mapStateToProps = ({ network, user, hasTokenListener }) => ({
  user,
  network,
  hasTokenListener,
});

export default connect(mapStateToProps, {
  changeNetworkStatus, syncMeasurements, startRefreshTokenListener,
})(AppNavigation);
