import { createMaterialTopTabNavigator } from 'react-navigation';
import { LastObservationsScreen, ObservationScreen } from '../../screens';
import I18n from '../../i18n';

export default createMaterialTopTabNavigator(
  {
    LastObservationsScreen: {
      screen: LastObservationsScreen,
      navigationOptions: { tabBarLabel: I18n.t('latest_tab') },
    },
    ObservationSelectScreen: {
      screen: ObservationScreen,
      navigationOptions: { tabBarLabel: I18n.t('all_tab') },
    },
  },
  {
    initialRouteName: 'ObservationSelectScreen',
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: 'black',
      },
      activeTintColor: 'black',
      inactiveTintColor: 'rgba(0, 0, 0, 0.61)',
      style: {
        backgroundColor: '#7EC667',
      },
    },
  },
);
