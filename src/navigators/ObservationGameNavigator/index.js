import { createMaterialTopTabNavigator } from 'react-navigation';
import { ObservationScreen } from '../../screens';
import Toplist from '../../components/Toplist';

export default createMaterialTopTabNavigator(
  {
    ObservationSelectScreen: {
      screen: ObservationScreen,
      navigationOptions: { tabBarLabel: 'Játék' },
    },
    Toplist: {
      screen: Toplist,
      navigationOptions: { tabBarLabel: 'Toplista' },
    },
  },
  {
    initialRouteName: 'ObservationSelectScreen',
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: 'black',
      },
      activeTintColor: 'black',
      inactiveTintColor: 'rgba(0, 0, 0, 0.61)',
      style: {
        backgroundColor: '#7EC667',
      },
    },
  },
);
