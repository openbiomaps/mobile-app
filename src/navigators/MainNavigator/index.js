import { createStackNavigator } from 'react-navigation';
import {
  ObservationMapScreen,
  CreateMeasurementScreen,
  EditMeasurementScreen,
  PositionInputScreen,
  ObservationCardsScreen,
  TaskCompletedScreen,
} from '../../screens';

export default createStackNavigator(
  {
    ObservationMapScreen: {
      screen: ObservationMapScreen,
    },
    CreateMeasurementScreen: {
      screen: CreateMeasurementScreen,
    },
    EditMeasurementScreen: {
      screen: EditMeasurementScreen,
    },
    PositionInputScreen: {
      screen: PositionInputScreen,
    },
    ObservationCardsScreen: {
      screen: ObservationCardsScreen,
    },
    TaskCompletedScreen: {
      screen: TaskCompletedScreen,
    },
  },
  {
    mode: 'modal',
    cardStyle: { backgroundColor: 'white' },
    initialRouteName: 'ObservationMapScreen',
  },
);
