import React from 'react';
import { TouchableWithoutFeedback, View, Text } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { createStackNavigator } from 'react-navigation';
import I18n from '../../i18n';

import DatabaseListNavigator from '../DatabaseListNavigator';
import ObservationListNavigator from '../ObservationListNavigator';
import ObservationGameNavigator from '../ObservationGameNavigator';

import { QuizInformationScreen, QuizQuestionsScreen, QuizTaskScreen, TaskCompletedScreen } from '../../screens';

import { headerStyle, titleStyle, headerTitleStyle } from '../../components';

export default createStackNavigator(
  {
    DatabaseListScreen: {
      screen: DatabaseListNavigator,
      navigationOptions: ({ navigation }) => ({
        headerLeft: (
          <TouchableWithoutFeedback
            onPress={navigation.state.params && navigation.state.params.toggleMenu}
          >
            <Icon name="menu" style={headerTitleStyle.button} />
          </TouchableWithoutFeedback>
        ),
        headerTitle: (
          <View style={headerTitleStyle.container}>
            <Text style={headerTitleStyle.title} numberOfLines={1}>{navigation.state.params ? `${navigation.state.params.title}` : ''}</Text>
            <Text style={headerTitleStyle.description}>{I18n.t('database_screen_title')}</Text>
          </View>
        ),
        headerStyle,
        headerTitleStyle: titleStyle,
      }),
    },
    ObservationScreen: {
      screen: ObservationListNavigator,
      navigationOptions: ({ navigation }) => ({
        headerTitle: (
          <View style={headerTitleStyle.container}>
            <Text style={headerTitleStyle.title} numberOfLines={1}>{navigation.state.params ? `${navigation.state.params.title}` : ''}</Text>
            <Text style={headerTitleStyle.description}>{I18n.t('observation_screen_title')}</Text>
          </View>
        ),
        headerStyle,
        headerTitleStyle: titleStyle,
      }),
    },
    ObservationTabScreen: {
      screen: ObservationGameNavigator,
      navigationOptions: {
        title: I18n.t('observation_screen_title'),
        headerStyle,
        headerTitleStyle: titleStyle,
      },
    },
    QuizInformationScreen: {
      screen: QuizInformationScreen,
    },
    QuizTaskScreen: {
      screen: QuizTaskScreen,
    },
    QuizQuestionsScreen: {
      screen: QuizQuestionsScreen,
    },
    TaskCompletedScreen: {
      screen: TaskCompletedScreen,
    },
  },
  {
    mode: 'modal',
    initialRouteName: 'DatabaseListScreen',
  },
);
