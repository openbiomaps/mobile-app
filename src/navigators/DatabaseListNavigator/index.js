import { createMaterialTopTabNavigator } from 'react-navigation';
import { DatabaseListScreen, LastDatabasesScreen } from '../../screens';
import I18n from '../../i18n';

export default createMaterialTopTabNavigator(
  {
    LastDatabasesScreen: {
      screen: LastDatabasesScreen,
      navigationOptions: { tabBarLabel: I18n.t('latest_tab') },
    },
    DatabaseListScreen: {
      screen: DatabaseListScreen,
      navigationOptions: { tabBarLabel: I18n.t('all_tab') },
    },
  },
  {
    initialRouteName: 'DatabaseListScreen',
    tabBarOptions: {
      indicatorStyle: {
        backgroundColor: 'black',
      },
      activeTintColor: 'black',
      inactiveTintColor: 'rgba(0, 0, 0, 0.61)',
      style: {
        backgroundColor: '#7EC667',
      },
    },
  },
);
