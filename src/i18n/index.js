import I18n from 'react-native-i18n';
import hu from './locales/hu';

I18n.fallbacks = true;

I18n.translations = {
  hu,
};

I18n.defaultLocale = 'hu';

export default I18n;
