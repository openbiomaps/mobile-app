import React from 'react';
import moment from 'moment';
import Image from '../components/HTMLImage';

export const getCenterOfPlot = (type, data) => {
  let middleIndex;
  let sumLatitude;
  let sumLongitude;
  switch (type) {
    case 'point':
      return data;
    case 'line':
      middleIndex = Math.floor(data.length / 2) - 1;
      return data[middleIndex];
    case 'area':
      sumLatitude = data.reduce((acc, curr) => acc + Number(curr.latitude), 0);
      sumLongitude = data.reduce((acc, curr) => acc + Number(curr.longitude), 0);
      return {
        latitude: sumLatitude / data.length,
        longitude: sumLongitude / data.length,
      };
    default:
      return null;
  }
};

export const parseWkt = (wkt) => {
  const point = /POINT\s*\(([^)]*)\)/;
  const line = /LINESTRING\s*\(([^)]*)\)/;
  const polygon = /POLYGON\s*\(\(([^)]*)\)(?:,\s*\(([^)]*)\))?\)/;
  const coordinate = /(\d+(?:\.\d+)?)\s+(\d+(?:\.\d+)?)/g;

  const getCoordinate = (data) => {
    const coordinate = /(-?\d+(?:\.\d+)?)\s(-?\d+(?:\.\d+)?)/g;
    const [, longitude, latitude] = coordinate.exec(data);
    return {
      latitude: Number(latitude),
      longitude: Number(longitude),
    };
  };

  let match = point.exec(wkt);
  if (match) {
    const [, innerText] = match;
    return {
      type: 'point',
      data: getCoordinate(innerText),
    };
  }

  const getCoordinates = data => data.match(coordinate).map(getCoordinate);

  match = line.exec(wkt);
  if (match) {
    const [, innerText] = match;
    return {
      type: 'line',
      data: getCoordinates(innerText),
    };
  }

  match = polygon.exec(wkt);
  if (match) {
    const [, area, ...holes] = match;
    return {
      type: 'area',
      data: getCoordinates(area),
      holes: holes ? holes.filter(hole => hole).map(hole => getCoordinates(hole)) : [],
    };
  }
  return null;
};

export const isRefreshTokenExpired = ({ requestTime, expiresIn }) => {
  const diff = moment(moment()).diff(requestTime);
  return diff > expiresIn;
};

export const renderNode = (node) => {
  if (node.name === 'img') {
    const a = node.attribs;
    return (
      <Image key={a.src} src={a.src} height={a.height} width={a.width} />
    );
  }
  return undefined;
};
