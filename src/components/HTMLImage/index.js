import React, { Component } from 'react';
import { Dimensions, Image, PixelRatio, Text } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import I18n from '../../i18n';

const PADDING = 20;
const isOffline = status => status === 'unknown' || status === 'none';

class HTMLImage extends Component {
  static propTypes = {
    src: PropTypes.string.isRequired,
    height: PropTypes.number,
    width: PropTypes.number,
    netStatus: PropTypes.string.isRequired,
  }

  static defaultProps = {
    height: null,
    width: null,
  }

  state = {}

  componentDidMount() {
    if (this.props.width && this.props.height) {
      this.setSize(this.props);
      return;
    }
    this.refreshImage();
  }

  componentDidUpdate(prevProps) {
    if (isOffline(prevProps.netStatus) && !isOffline(this.props.netStatus)) {
      this.refreshImage();
    }
  }

  setSize = ({ width, height }) => {
    const dimensions = Dimensions.get('window');
    const ratio = (PixelRatio.get() * (dimensions.width - PADDING)) / width;
    this.setState({ width: width * ratio, height: height * ratio });
  }

  getSize = () => {
    const { width, height } = this.state;
    return { width, height };
  }

  handleError = () => {
    this.setState({ error: true });
  }

  refreshImage = () => {
    this.setState({ error: false });
    Image.getSize(
      this.props.src,
      (width, height) => {
        this.setSize({ width, height });
      },
      (e) => {
        this.handleError();
        console.log(e);
      },
    );
  }

  render() {
    const { src, netStatus } = this.props;
    const size = this.getSize();
    if (this.state.error) {
      return (
        <Text key="img-error" style={{ color: '#f93636' }}>
          {I18n.t('image_load_failed')}
          {isOffline(netStatus) && `\n${I18n.t('image_needs_net')}`}
        </Text>
      );
    }
    return (this.state.width && this.state.height) ? (
      <Text>
        <Image resizeMode="cover" source={{ uri: src, ...size }} style={size} />
      </Text>
    ) : <Text style={{ color: '#888' }}>{I18n.t('img_loading')}</Text>;
  }
}

const mapStateToProps = ({ network: { status } }) => ({
  netStatus: status,
});

export default connect(mapStateToProps)(HTMLImage);
