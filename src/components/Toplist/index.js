import React, { Component } from 'react';
import { FlatList, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { loadToplist } from '../../actions';
import I18n from '../../i18n';
import styles from './styles';

class Toplist extends Component {
  static propTypes = {
    toplist: PropTypes.arrayOf(PropTypes.shape({
      name: PropTypes.string.isRequired,
      score: PropTypes.number.isRequired,
    })),
    loadToplist: PropTypes.func.isRequired,
    database: PropTypes.shape({}).isRequired,
    accessToken: PropTypes.string.isRequired,
    navigation: PropTypes.shape({}).isRequired,
  }

  static defaultProps = {
    toplist: [],
  }

  static navigationOptions = {
    tabBarLabel: I18n.t('toplist'),
  }

  componentDidMount() {
    const { navigation } = this.props;
    this.onNavigationFocus = navigation.addListener('willFocus', () => {
      this.loadToplist();
    });
  }

  componentWillUnmount() {
    this.onNavigationFocus.remove();
  }

  loadToplist = () => {
    const { loadToplist, database, accessToken } = this.props;
    loadToplist({ database, accessToken });
  }

  render() {
    const { toplist } = this.props;
    return (
      <FlatList
        contentContainerStyle={styles.container}
        ListHeaderComponent={
          <View style={styles.header}>
            <Text style={[styles.place, styles.headerText]} />
            <Text style={[styles.name, styles.headerText]}>{I18n.t('player')}</Text>
            <Text style={[styles.score, styles.headerText]}>{I18n.t('score')}</Text>
          </View>
        }
        keyExtractor={item => item.name}
        data={toplist}
        renderItem={({ item, index }) => (
          <View style={styles.row}>
            <Text style={styles.place}>{index + 1}</Text>
            <Text style={styles.name} ellipsizeMode="tail" numberOfLines={1}>{item.name}</Text>
            <Text style={styles.score}>{item.score}</Text>
          </View>
        )}
      />
    );
  }
}

const mapStateToProps = ({ databases, network, user: { accessToken } }) => {
  const database = databases.find(database => database.isSelected);
  return {
    database,
    toplist: database.toplist,
    netStatus: network.status,
    accessToken,
  };
};

export default connect(mapStateToProps, { loadToplist })(Toplist);
