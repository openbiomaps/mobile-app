import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    paddingHorizontal: 10,
  },
  row: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderBottomColor: '#e5e5e5',
    padding: 5,
  },
  place: {
    width: 35,
  },
  name: {
    flex: 1,
  },
  score: {
    textAlign: 'right',
  },
  header: {
    flexDirection: 'row',
    borderBottomWidth: 1,
    flex: 1,
    padding: 5,
    paddingVertical: 10,
  },
  headerText: {
    borderBottomColor: '#eee',
    fontWeight: 'bold',
  },
});
