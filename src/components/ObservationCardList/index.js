import React from 'react';
import { SectionList, Text, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import I18n from '../../i18n';
import ObservationCard from './ObservationCard';
import styles from './styles';

const keyExtractor = item => String(item.id);

const createSections = (data) => {
  const measurements = [...data];
  // newest first
  measurements.sort((a, b) => b.id - a.id);
  const nonvalidMeasurements = measurements.filter(measurement =>
    measurement.isSynced && measurement.errors && !measurement.isEditing);
  const syncingMeasurements = measurements.filter(measurement =>
    measurement.syncing);
  const pendingMeasurements = measurements.filter(measurement =>
    !measurement.isSynced && !measurement.syncing && !measurement.isEditing);
  const uploadedMeasurements = measurements.filter(measurement =>
    measurement.isSynced && !measurement.errors);
  const measurementsUnderEditing = measurements.filter(measurement =>
    measurement.isEditing);
  return [
    { title: I18n.t('measurement_under_editing'), data: measurementsUnderEditing, editable: true },
    { title: I18n.t('measurement_nonvalid'), data: nonvalidMeasurements, editable: true },
    { title: I18n.t('measurement_syncing'), data: syncingMeasurements },
    { title: I18n.t('measurement_pending'), data: pendingMeasurements, editable: true },
    { title: I18n.t('measurement_uploaded'), data: uploadedMeasurements },
  ];
};

const ObservationCardList = ({
  measurements,
  dataTypes,
  observationName,
  onPress,
}) => (
  <SectionList
    keyExtractor={keyExtractor}
    sections={createSections(measurements)}
    renderSectionHeader={({ section: { title, data } }) => (
      data.length ? <Text style={styles.sectionHeader}>{title}</Text> : null
    )}
    renderItem={({ item, section }) => (
      section.editable ?
        <TouchableOpacity onPress={() => onPress(item)}>
          <ObservationCard
            values={item.data}
            observationName={observationName}
            dataTypes={dataTypes}
          />
        </TouchableOpacity> :
        <ObservationCard
          values={item.data}
          observationName={observationName}
          dataTypes={dataTypes}
        />)}
  />
);

ObservationCardList.propTypes = {
  measurements: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  dataTypes: PropTypes.arrayOf(PropTypes.shape({
    column: PropTypes.string.isRequired,
    type: PropTypes.string.isRequired,
  })).isRequired,
  observationName: PropTypes.string.isRequired,
  onPress: PropTypes.func.isRequired,
};

export default ObservationCardList;
