import React, { PureComponent } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';
import MapComponent from '../Map';
import MeasurementInfo from '../MeasurementInfo';

const getRegion = (measurements) => {
  const positions = measurements.map(measurement => measurement.coordinate);
  const longitudes = positions.map(position => position.longitude);
  const latitudes = positions.map(position => position.latitude);
  const maxLatitude = Math.max(...latitudes);
  const minLatitude = Math.min(...latitudes);
  const maxLongitude = Math.max(...longitudes);
  const minLongitude = Math.min(...longitudes);
  switch (positions.length) {
    case 0:
      return null;
    case 1:
      return {
        ...positions[0],
        // distance of the edges of the map in degrees
        // too low values make the map pixelated
        latitudeDelta: 0.0005,
        longitudeDelta: 0.006,
      };
    default:
      return {
        latitude: (maxLatitude + minLatitude) / 2,
        longitude: (maxLongitude + minLongitude) / 2,
        latitudeDelta: Math.max(maxLatitude - minLatitude, 0.0005),
        longitudeDelta: Math.max(maxLongitude - minLongitude, 0.006),
      };
  }
};

export default class ObservationCard extends PureComponent {
  static propTypes = {
    values: PropTypes.shape({}).isRequired,
    observationName: PropTypes.string.isRequired,
    dataTypes: PropTypes.array.isRequired,
  };

  render() {
    const {
      dataTypes,
      values,
      observationName,
    } = this.props;
    const positions = dataTypes
      .filter(data => (data.type === 'point' || data.type === 'wkt') && values[data.column])
      .map(data => ({
        id: data.column,
        coordinate: values[data.column],
      }));

    const region = getRegion(positions);

    return (
      <View style={styles.container}>
        {region &&
        <View style={styles.mapContainer}>
          <MapComponent style={styles.map} region={region} liteMode lastMeasurements={positions} />
          <View style={styles.mask} pointerEvents="none" />
        </View>}
        <View style={styles.description}>
          <MeasurementInfo dataTypes={dataTypes} values={values} observation={observationName} />
        </View>
      </View>
    );
  }
}
