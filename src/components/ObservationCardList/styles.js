import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    elevation: 2,
    borderRadius: 20,
    width: '90%',
    alignSelf: 'center',
    margin: 20,
  },
  mapContainer: {
    height: 200,
  },
  description: {
    padding: 15,
  },
  map: {
    position: 'absolute',
    top: 2,
    right: 1,
    left: 1,
    bottom: 0,
  },
  mask: {
    position: 'absolute',
    top: -20,
    bottom: 0,
    right: -20,
    left: -20,
    borderTopRightRadius: 40,
    borderTopLeftRadius: 40,
    borderWidth: 20,
    borderBottomWidth: 0,
    borderColor: 'white',
    elevation: 2,
  },
  sectionHeader: {
    fontWeight: 'bold',
    fontSize: 16,
    paddingLeft: 30,
    marginTop: 15,
  },
});
