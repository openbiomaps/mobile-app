import React from 'react';
import { Text, TextInput, View, TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/SimpleLineIcons';
import {
  // CRings,
  DatePicker,
  PositionInput,
  ImagePicker,
  List,
  AutoComplete,
  Boolean,
} from './Form';
import { MeasurementFormListItem as styles } from './styles';

const renderItem = (item, onChange) => {
  switch (item.type) {
    // case 'crings':
    //   return <CRings data={item.list} values={item.value} onValueChange={onChange} />;
    case 'date':
      return <DatePicker value={item.value} onValueChange={onChange} dateOnly />;
    case 'time':
      return <DatePicker value={item.value} onValueChange={onChange} timeOnly />;
    case 'datetime':
      return <DatePicker value={item.value} onValueChange={onChange} />;
    case 'point':
    case 'wkt':
      return <PositionInput column={item.column} position={item.value} />;
    case 'file_id':
      return <ImagePicker items={item.value} onChange={onChange} />;
    case 'text':
      return <TextInput value={item.value} onChangeText={onChange} style={styles.textInput} />;
    case 'numeric':
      return <TextInput value={item.value} onChangeText={onChange} keyboardType="numeric" style={styles.textInput} />;
    case 'list':
      return <List data={item.list} onValueChange={onChange} value={item.value} />;
    case 'autocomplete':
      return (
        <AutoComplete
          value={item.value}
          data={Array.isArray(item.genlist) ? item.genlist.filter(listItem =>
            listItem).map(listItem => (typeof listItem === 'string' ? listItem : Object.values(listItem)[0])) : []}
          onValueChange={onChange}
        />);
    case 'boolean':
      return <Boolean value={item.value} onValueChange={onChange} />;
    default:
      return <TextInput value={item.value} onChangeText={onChange} style={styles.textInput} />;
  }
};

const renderRequired = ({ obl, error }) => (obl === '1' || obl === '3') && (
  <View style={styles.requiredContainer}>
    <Text style={[styles.requiredText, error ? styles.error : null]}> *</Text>
  </View>
);

const checkIfHidden = ({ type, list }) => (
  type === 'list' && (Array.isArray(list) ? list.length <= 1 : Object.keys(list).length <= 1)
);

const ListItem = ({ item, onChange, onPinPress }) => (
  <View style={checkIfHidden(item) ? styles.hidden : styles.item}>
    <View style={styles.row}>
      <Text style={[item.error ? styles.error : undefined]}>{item.shortName}</Text>
      {renderRequired(item)}
      <TouchableOpacity onPress={onPinPress}>
        <View style={styles.stickyButton}>
          <Icon
            name="pin"
            style={item.isSticky ? styles.rotated : null}
          />
        </View>
      </TouchableOpacity>
    </View>
    {renderItem(item, onChange)}
    {item.error && <Text style={[styles.error, styles.row]}>{item.error}</Text>}
  </View>
);

ListItem.propTypes = {
  item: PropTypes.shape({}).isRequired,
  onChange: PropTypes.func.isRequired,
  onPinPress: PropTypes.func.isRequired,
};

export default ListItem;
