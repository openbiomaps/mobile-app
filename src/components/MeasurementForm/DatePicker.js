import React from 'react';
import { Text, View, DatePickerAndroid, TimePickerAndroid, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import moment from 'moment';
import { DatePicker as styles } from './styles';

const onDatePress = async (val, onValueChange) => {
  const value = val ? new Date(val) : new Date();
  try {
    const {
      action,
      year,
      month,
      day,
    } = await DatePickerAndroid.open({
      date: value,
    });
    if (action !== DatePickerAndroid.dismissedAction) {
      value.setFullYear(year);
      value.setMonth(month);
      value.setDate(day);
      onValueChange(value);
    }
  } catch ({ message }) {
    console.warn('Cannot open date picker', message);
  }
};

const onTimePress = async (val, onValueChange) => {
  const value = val ? new Date(val) : new Date();
  try {
    const { action, hour, minute } = await TimePickerAndroid.open({
      hour: value.getHours(),
      minute: value.getMinutes(),
    });
    if (action !== TimePickerAndroid.dismissedAction) {
      value.setHours(hour);
      value.setMinutes(minute);
      onValueChange(value);
    }
  } catch ({ message }) {
    console.warn('Cannot open time picker', message);
  }
};

const getFormattedDateTime = (value, dateOnly, timeOnly) => {
  if (!value) {
    return '-';
  }
  const date = moment(value).format('YYYY.MM.DD');
  const time = moment(value).format('HH:mm');
  if (dateOnly) {
    return date;
  }
  if (timeOnly) {
    return time;
  }
  return `${date} ${time}`;
};

const DateTimePicker = ({
  value,
  onValueChange,
  dateOnly,
  timeOnly,
}) => (
  <View style={styles.container}>
    <Text style={styles.text}>
      {getFormattedDateTime(value, dateOnly, timeOnly)}
    </Text>
    {!dateOnly &&
    <TouchableWithoutFeedback onPress={() => onTimePress(value, onValueChange)}>
      <View style={styles.buttonContainer}>
        <Icon name="access-time" color="rgba(0, 0, 0, 0.6)" size={20} />
      </View>
    </TouchableWithoutFeedback>}
    {!timeOnly &&
    <TouchableWithoutFeedback onPress={() => onDatePress(value, onValueChange)}>
      <View style={styles.buttonContainer}>
        <Icon name="event" color="rgba(0, 0, 0, 0.6)" size={20} style={styles.dateButton} />
      </View>
    </TouchableWithoutFeedback>}
  </View>
);

DateTimePicker.propTypes = {
  value: PropTypes.instanceOf(Date),
  onValueChange: PropTypes.func.isRequired,
  dateOnly: PropTypes.bool,
  timeOnly: PropTypes.bool,
};

DateTimePicker.defaultProps = {
  value: null,
  dateOnly: false,
  timeOnly: false,
};

export default DateTimePicker;
