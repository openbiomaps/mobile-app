import React, { Component } from 'react';
import { ActivityIndicator, View, TouchableWithoutFeedback, Text } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import I18n from '../../i18n';
import { createMeasurement, setStickinesses, syncMeasurement, navigateBack, resetStack } from '../../actions';
import { footerStyle, SignalIcon } from '../../components';
import Form from './Form';
import { FormContainer as styles } from './styles';

class MeasurementForm extends Component {
  static propTypes = {
    id: PropTypes.number.isRequired,
    observation: PropTypes.shape({}).isRequired,
    database: PropTypes.shape({}).isRequired,
    form: PropTypes.array.isRequired,
    lastMeasurement: PropTypes.shape({}),
    errors: PropTypes.shape({}),
    values: PropTypes.shape({}),
    stickinesses: PropTypes.shape({}),
    navigation: PropTypes.shape({}).isRequired,
    syncMeasurement: PropTypes.func.isRequired,
    setStickinesses: PropTypes.func.isRequired,
    onDonePress: PropTypes.func.isRequired,
    navigateBack: PropTypes.func.isRequired,
  }

  static defaultProps = {
    lastMeasurement: {},
    errors: {},
    values: {},
    stickinesses: null,
  }

  constructor(props) {
    super(props);
    const {
      form,
      lastMeasurement,
      errors,
    } = props;

    const stickinesses = {
      ...props.stickinesses,
    };

    const values = {};

    if (form) {
      // form.filter(item => item.type === 'crgins').forEach((item) => {
      //   values[item.column] = [];
      // });

      form.filter(item => item.type === 'list').forEach((item) => {
        const { list } = item;
        values[item.column] = Array.isArray(list) ?
          list[0] : Object.values(list)[0];
      });

      if (props.stickinesses) {
        form.forEach(({ column }) => {
          if (stickinesses[column] && lastMeasurement[column]) {
            values[column] = lastMeasurement[column];
          }
        });
      } else {
        form.forEach((item) => {
          stickinesses[item.column] = item.apiParams && item.apiParams.includes('sticky');
        });
      }

      this.state = {
        values: {
          ...values,
          ...props.values,
        },
        errors,
        stickinesses,
      };
    }
  }

  static getDerivedStateFromProps = (props, state) => {
    const positionInputCoordinates = props.navigation.getParam('positionInputCoordinates', {});
    if (Object.keys(positionInputCoordinates).length === 0) {
      return null;
    }
    props.navigation.setParams({
      positionInputCoordinates: {},
    });
    return ({
      values: {
        ...state.values,
        ...positionInputCoordinates,
      },
    });
  }

  handleValueChange = (key, value) => {
    this.setState((prevState) => {
      const values = { ...prevState.values, [key]: value };
      return ({ values, showErrorBar: false });
    });
  }

  handleDonePress = async () => {
    this.setState({ isLoading: true });
    const {
      id,
      form,
      database,
      observation,
      setStickinesses,
      syncMeasurement,
      navigation,
      navigateBack,
      onDonePress,
    } = this.props;
    const { stickinesses, values } = this.state;
    if (this.validateFields()) {
      setStickinesses(database.id, observation.id, stickinesses);
      const measurement = { ...values, id };

      onDonePress(measurement);

      syncMeasurement({
        dataTypes: form,
        database,
        observation,
        measurement,
      });

      if (observation.game) {
        navigation.dispatch(resetStack('TaskCompletedScreen'));
      } else {
        navigation.dispatch(navigateBack());
      }
    }
  }

  handlePinPress = (column) => {
    this.setState(prevState => ({
      stickinesses: {
        ...prevState.stickinesses,
        [column]: !prevState.stickinesses[column],
      },
    }));
  }

  validateFields = () => {
    let isValid = true;

    const { form } = this.props;
    const { values } = this.state;
    const errors = {};

    const minMax = form.filter(item => item.control === 'minmax');
    minMax.forEach((item) => {
      const { column } = item;
      const [, minValue, maxValue] = /{(-?\d+(?:\.\d+)*),(-?\d+(?:\.\d+)*)}/.exec(item.count);
      const value = values[column];
      if (value && item.type === 'text' && (value.length < minValue || value.length > maxValue)) {
        errors[column] = I18n.t('error_text_out_of_range', { min: minValue, max: maxValue });
        isValid = false;
      }
      if (value && item.type === 'numeric' && (Number(value) < minValue || Number(value) > maxValue)) {
        errors[column] = I18n.t('error_number_out_of_range', { min: minValue, max: maxValue });
        isValid = false;
      }
    });

    form.filter(item => item.type === 'numeric').forEach((item) => {
      const value = values[item.column];
      if (value && isNaN(value)) {
        errors[item.column] = I18n.t('error_nan');
        isValid = false;
      }
    });

    const required = form.filter(item => item.obl === '1' || item.obl === '3');
    required.forEach(({ column, type }) => {
      const value = values[column];
      if (typeof value === 'undefined' || (type === 'file_id' && value.length === 0)) {
        errors[column] = I18n.t('error_required');
        isValid = false;
      }
    });

    this.setState({ errors, isLoading: false, showErrorBar: !isValid });
    return isValid;
  }

  renderErrorBar = () => {
    const { errors } = this.props;
    const { showErrorBar } = this.state;
    if (showErrorBar) {
      return <Text style={styles.error}>{I18n.t('invalid_fields')}</Text>;
    }
    if (errors.errorMessage) {
      return <Text style={styles.error}>{errors.errorMessage}</Text>;
    }
    if (Object.keys(errors).length) {
      return <Text style={styles.error}>{I18n.t('invalid_fields')}</Text>;
    }
    return null;
  }

  render() {
    const { form } = this.props;
    return (
      form ? (
        <View style={styles.container}>
          {this.renderErrorBar()}
          <Form
            form={form}
            values={this.state.values}
            errors={this.state.errors ? this.state.errors : {}}
            onValueChange={this.handleValueChange}
            stickinesses={this.state.stickinesses}
            onPinPress={this.handlePinPress}
          />
          <View style={styles.buttonContainer}>
            {this.state.isLoading ?
              <View style={styles.doneButton}>
                <ActivityIndicator style={styles.spinner} color="white" />
              </View> :
              <TouchableWithoutFeedback onPress={this.handleDonePress}>
                <View style={styles.doneButton}>
                  <Icon name="done" size={24} style={styles.doneIcon} />
                </View>
              </TouchableWithoutFeedback>
            }
          </View>
          <View style={footerStyle.container}>
            <SignalIcon />
          </View>
        </View>
      ) :
        <View style={[styles.container, styles.centered]}>
          <Icon name="cloud-off" size={100} />
          <Text style={styles.missingFormError}>{I18n.t('missing_form_data')}</Text>
        </View>
    );
  }
}

const mapStateToProps = ({ databases }) => {
  const database = databases.find(database => database.isSelected);
  const observation = database && database.observations.find(observation => observation.isSelected);
  const form = observation && observation.form && observation.form.dataTypes;
  const stickinesses = observation && observation.form && observation.form.stickinesses;
  const measurements = observation &&
    (observation.measurements ? [...observation.measurements] : []);
  measurements.sort((a, b) => b.id - a.id);
  const lastMeasurement = measurements.length ? measurements[0].data : {};
  return ({
    database,
    observation,
    form,
    lastMeasurement,
    stickinesses,
  });
};

export default connect(
  mapStateToProps,
  {
    createMeasurement, setStickinesses, syncMeasurement, navigateBack,
  },
)(MeasurementForm);
