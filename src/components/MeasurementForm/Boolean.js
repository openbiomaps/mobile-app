import React from 'react';
import { TouchableOpacity } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { Boolean as styles } from './styles';

const getIconName = (value) => {
  if (value === true) {
    return 'check-box';
  }
  if (value === false) {
    return 'check-box-outline-blank';
  }
  return 'indeterminate-check-box';
};

const Boolean = ({ value, onValueChange }) => (
  <TouchableOpacity onPress={() => onValueChange(!value)} style={styles.container} >
    <Icon size={25} color="#7EC667" name={getIconName(value)} />
  </TouchableOpacity>
);

Boolean.propTypes = {
  value: PropTypes.bool,
  onValueChange: PropTypes.func.isRequired,
};

Boolean.defaultProps = {
  value: null,
};

export default Boolean;
