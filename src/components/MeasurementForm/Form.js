import React from 'react';
import { FlatList } from 'react-native';
import PropTypes from 'prop-types';
import ListItem from './MeasurementFormListItem';
import { MeasurementForm as styles } from './styles';

const MeasurementForm = ({
  form,
  values,
  onValueChange,
  errors,
  stickinesses,
  onPinPress,
}) => {
  const data = form.map(item => ({
    ...item,
    value: values[item.column],
    error: errors[item.column],
    isSticky: stickinesses[item.column],
  }));
  return (
    <FlatList
      keyboardShouldPersistTaps="handled"
      contentContainerStyle={styles.container}
      data={data}
      keyExtractor={item => item.column}
      renderItem={({ item }) => (
        <ListItem
          item={item}
          onChange={value => onValueChange(item.column, value)}
          onPinPress={() => onPinPress(item.column)}
        />
      )}
    />
  );
};

MeasurementForm.propTypes = {
  form: PropTypes.array.isRequired,
  values: PropTypes.shape({}).isRequired,
  onValueChange: PropTypes.func.isRequired,
  errors: PropTypes.shape({}).isRequired,
  stickinesses: PropTypes.shape({}).isRequired,
  onPinPress: PropTypes.func.isRequired,
};

export default MeasurementForm;

export { default as CRings } from './CRings';
export { default as DatePicker } from './DatePicker';
export { default as ImagePicker } from './ImagePicker';
export { default as PositionInput } from './PositionInput';
export { default as List } from './List';
export { default as AutoComplete } from './AutoComplete';
export { default as Boolean } from './Boolean';
