import React from 'react';
import { View, Text, TouchableWithoutFeedback } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { withNavigation } from 'react-navigation';
import { navigateTo } from '../../actions';
import { PositionInput as styles } from './styles';

const onPress = (column, position, navigation) => {
  const { key } = navigation.state;
  navigation.dispatch(navigateTo('PositionInputScreen', { column, key, position }));
};

const PositionInput = ({ position, column, navigation }) => (
  <TouchableWithoutFeedback onPress={() => onPress(column, position, navigation)}>
    <View style={styles.container}>
      <View style={styles.button}>
        <Icon name="place" size={20} style={styles.buttonIcon} />
      </View>
      <View style={styles.field}>
        <Text numberOfLines={1} >{position ? `${position.latitude}, ${position.longitude}` : '-'}</Text>
      </View>
    </View>
  </TouchableWithoutFeedback>
);

PositionInput.defaultProps = {
  position: null,
};

PositionInput.propTypes = {
  position: PropTypes.shape({
    longitude: PropTypes.number.isRequired,
    latitude: PropTypes.number.isRequired,
  }),
  column: PropTypes.string.isRequired,
  navigation: PropTypes.shape({}).isRequired,
};

export default withNavigation(PositionInput);
