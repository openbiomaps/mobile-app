import { StyleSheet } from 'react-native';

export const CRings = StyleSheet.create({
  container: {
    width: '100%',
  },
  row: {
    flexDirection: 'row',
    marginLeft: 15,
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    paddingVertical: 5,
    paddingRight: 20,
    paddingLeft: 0,
    alignItems: 'center',
    width: '100%',
  },
  text: {
    flex: 1,
  },
});

export const DatePicker = StyleSheet.create({
  container: {
    marginHorizontal: 15,
    paddingHorizontal: 15,
    backgroundColor: '#F5F5F5',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  dateButton: {
    marginLeft: 5,
  },
  text: {
    flex: 1,
    paddingVertical: 15,
    color: 'rgba(0, 0, 0, 0.6)',
  },
  buttonContainer: {
    paddingVertical: 15,
  },
});

export const PositionInput = StyleSheet.create({
  container: {
    width: '100%',
    paddingHorizontal: 30,
    paddingVertical: 15,
  },
  field: {
    backgroundColor: '#F1F1F1',
    borderRadius: 15,
    paddingVertical: 5,
    paddingLeft: 35,
    paddingRight: 15,
    alignItems: 'center',
    width: '100%',
  },
  text: {
    marginLeft: 25,
  },
  buttonIcon: {
    transform: [{ rotate: '-45deg' }],
    color: 'white',
  },
  button: {
    backgroundColor: '#434A54',
    margin: 10,
    padding: 7,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 3,
    position: 'absolute',
    left: 10,
    top: 2,
  },
});

export const ImagePicker = StyleSheet.create({
  container: {
    padding: 15,
    width: '100%',
    backgroundColor: '#F1F1F1',
  },
  button: {
    marginTop: 15,
    alignSelf: 'flex-end',
  },
});

export const ImagePickerListItem = StyleSheet.create({
  container: {
    height: 200,
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    marginTop: 3,
  },
  removeButton: {
    position: 'absolute',
    bottom: 10,
    right: 10,
    padding: 5,
    borderRadius: 5,
    backgroundColor: 'rgba(255,255,255,.4)',
  },
  removeText: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#F4FAFE',
  },
  image: {
    borderRadius: 10,
    width: '100%',
    height: '100%',
  },
});

export const MeasurementForm = StyleSheet.create({
  container: {
    paddingBottom: 45,
    width: '100%',
  },
});

export const MeasurementFormListItem = StyleSheet.create({
  row: {
    marginHorizontal: 20,
    marginVertical: 10,
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInput: {
    marginHorizontal: 20,
  },
  item: {
    marginVertical: 10,
  },
  stickyButton: {
    padding: 5,
  },
  rotated: {
    transform: [{ rotate: '-45deg' }],
  },
  error: {
    color: '#f44242',
  },
  requiredContainer: {
    height: 12,
    alignItems: 'flex-start',
    justifyContent: 'flex-start',
  },
  requiredText: { fontSize: 12, fontWeight: 'bold' },
  hidden: { display: 'none' },
});

export const List = StyleSheet.create({
  list: {
    marginHorizontal: 15,
  },
});

export const AutoComplete = StyleSheet.create({
  container: {
    marginHorizontal: 15,
    paddingHorizontal: 15,
    height: 40,
    backgroundColor: '#F1F1F1',
    justifyContent: 'center',
  },
  header: {
    padding: 15,
    elevation: 3,
    backgroundColor: '#FAFAFA',
    flexDirection: 'row',
  },
  listItem: {
    borderBottomColor: 'lightgrey',
    borderBottomWidth: 1,
    padding: 15,
    marginLeft: 10,
  },
  textInput: {
    flex: 1,
  },
  button: {
    justifyContent: 'center',
  },
});

export const Boolean = StyleSheet.create({
  container: {
    position: 'absolute',
    top: 10,
    right: 15,
  },
});

export const FormContainer = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  buttonContainer: {
    position: 'absolute',
    bottom: 20,
    right: 10,
  },
  doneIcon: {
    transform: [{ rotate: '-45deg' }],
    color: 'white',
  },
  doneButton: {
    backgroundColor: '#434A54',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 3,
  },
  error: {
    paddingLeft: 20,
    paddingVertical: 15,
    color: 'white',
    backgroundColor: '#f93636',
  },
  centered: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  missingFormError: {
    fontSize: 20,
    fontWeight: 'bold',
  },
});
