import React, { Component } from 'react';
import { View, Text, Modal, TextInput, TouchableOpacity, FlatList, ScrollView } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ListItem from './AutoCompleteListItem';
import { AutoComplete as styles } from './styles';

class AutoComplete extends Component {
  constructor(props) {
    super(props);
    this.state = { visible: false };
  }

  handlePress = () => {
    this.toggleModal();
  }

  handleModalShow = () => {
    this.textInput.focus();
  }

  handleItemPress = (value) => {
    this.props.onValueChange(value);
    this.toggleModal();
  }

  handleChangeText = (textValue) => {
    this.props.onValueChange(textValue);
  }

  toggleModal = () => {
    this.setState(prevState => ({ visible: !prevState.visible }));
  }

  render() {
    const { value } = this.props;
    const list = value.length > 0 ? this.props.data.filter(item =>
      item && item.toLowerCase().startsWith(value.toLowerCase())) : [];
    return (
      <View>
        <TouchableOpacity onPress={this.handlePress} style={styles.container}>
          <Text>{value}</Text>
        </TouchableOpacity>
        <Modal
          presentationStyle="fullScreen"
          visible={this.state.visible}
          onRequestClose={this.toggleModal}
          animationType="slide"
          transparent={false}
          onShow={this.handleModalShow}
        >
          <View style={styles.header}>
            <TextInput
              ref={(input) => { this.textInput = input; }}
              value={value}
              onChangeText={this.handleChangeText}
              blurOnSubmit
              onSubmitEditing={this.toggleModal}
              underlineColorAndroid="lightgrey"
              style={styles.textInput}
            />
            <TouchableOpacity onPress={this.toggleModal} style={styles.button}>
              <Icon name="close" size={20} onPress={this.toggleModal} />
            </TouchableOpacity>
          </View>
          <ScrollView keyboardShouldPersistTaps="handled">
            <FlatList
              keyboardShouldPersistTaps="handled"
              data={list}
              keyExtractor={item => item}
              renderItem={({ item }) =>
                <ListItem key={item} value={item} onPress={() => this.handleItemPress(item)} />
              }
            />
          </ScrollView>
        </Modal>
      </View>
    );
  }
}

AutoComplete.propTypes = {
  data: PropTypes.array.isRequired,
  value: PropTypes.string,
  onValueChange: PropTypes.func.isRequired,
};

AutoComplete.defaultProps = {
  value: '',
};

export default AutoComplete;
