import React from 'react';
import { View, Text, TouchableOpacity, Image } from 'react-native';
import PropTypes from 'prop-types';
import I18n from '../../i18n';
import { ImagePickerListItem as styles } from './styles';

const ImagePickerListItem = ({
  id,
  uri,
  onRemove,
}) => (
  <View
    style={styles.container}
  >
    <Image style={styles.image} source={{ uri }} />
    <TouchableOpacity
      style={styles.removeButton}
      onPress={() => onRemove(id)}
    >
      <Text style={styles.removeText}>{I18n.t('remove')}</Text>
    </TouchableOpacity>
  </View>
);

ImagePickerListItem.propTypes = {
  id: PropTypes.number.isRequired,
  uri: PropTypes.string.isRequired,
  onRemove: PropTypes.func.isRequired,
};

export default ImagePickerListItem;
