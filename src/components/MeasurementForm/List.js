import React from 'react';
import { Picker } from 'react-native';
import PropTypes from 'prop-types';
import { List as styles } from './styles';

const renderLabeledItem = (label, value) => {
  const index = label.indexOf('#');
  const parsedLabel = index === -1 ? label : label.substring(0, index);
  return <Picker.Item key={value} label={parsedLabel} value={value} />;
};

const renderItems = data => (
  Array.isArray(data) ?
    data.map(item => <Picker.Item key={item} label={item} value={item} />) :
    Object.keys(data).map(item => renderLabeledItem(item, data[item]))
);

const List = ({ data, value, onValueChange }) => (
  <Picker
    style={styles.list}
    selectedValue={value}
    onValueChange={onValueChange}
  >
    {renderItems(data)}
  </Picker>
);

List.propTypes = {
  data: PropTypes.shape({}).isRequired,
  value: PropTypes.any,
  onValueChange: PropTypes.func.isRequired,
};

List.defaultProps = {
  value: null,
};

export default List;
