import React, { Component } from 'react';
import {
  View,
  Alert,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import PropTypes from 'prop-types';
import { Button } from 'react-native-vector-icons/MaterialIcons';
import ImagePicker from 'react-native-image-picker';
import I18n from '../../i18n';
import ListItem from './ImagePickerListItem';
import { ImagePicker as styles } from './styles';

class ImagePickerComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      indicator: false,
    };
  }

  addItem = ({ uri, type, fileName }) => {
    const { items } = this.props;
    const id = Math.max(0, ...items.map(item => item.id)) + 1;
    const newItems = [{
      id,
      uri,
      type,
      fileName,
    }, ...items];
    this.props.onChange(newItems);
  }

  removeItem = (id) => {
    const { items } = this.props;
    const newItems = items.filter(item => item.id !== id);
    this.props.onChange(newItems);
  }

  showIndicator = () => {
    this.setState({ indicator: true });
  }

  hideIndicator = () => {
    this.setState({ indicator: false });
  }

  handleAddItemPress = () => {
    const options = {
      title: I18n.t('image_picker_title'),
      cancelButtonTitle: I18n.t('cancel'),
      takePhotoButtonTitle: I18n.t('camera'),
      chooseFromLibraryButtonTitle: I18n.t('gallery'),
      mediaType: 'photo',
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    this.showIndicator();
    ImagePicker.showImagePicker(options, (response) => {
      if (response.error) {
        this.hideIndicator();
        Alert.alert(response.error);
      } else if (response.didCancel) {
        this.hideIndicator();
      } else {
        this.hideIndicator();
        this.addItem(response);
      }
    });
  }

  render() {
    if (this.state.indicator) {
      return <ActivityIndicator size="large" />;
    }
    return (
      <View style={styles.container}>
        <FlatList
          data={this.props.items}
          keyExtractor={item => String(item.id)}
          renderItem={({ item }) => (
            <ListItem {...item} onRemove={this.removeItem} />
          )}
        />
        <View style={styles.button}>
          <Button
            name="add-a-photo"
            backgroundColor="#2A9B44"
            onPress={this.handleAddItemPress}
          />
        </View>
      </View>
    );
  }
}

ImagePickerComponent.defaultProps = {
  items: [],
};

ImagePickerComponent.propTypes = {
  items: PropTypes.array,
  onChange: PropTypes.func.isRequired,
};

export default ImagePickerComponent;
