import React from 'react';
import MaterialTextInput from 'react-native-material-textinput';

const styleProps = {
  marginBottom: 0,
  underlineHeight: 2,
  labelActiveColor: '#7EC667',
  underlineActiveColor: '#7EC667',
  paddingLeft: 15,
  paddingRight: 15,
  paddingTop: 25,
  labelColor: '#222',
  underlineColorAndroid: 'transparent',
};

const TextInput = props => <MaterialTextInput {...styleProps} {...props} />;

export { TextInput };

