import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Modal, TouchableWithoutFeedback, Linking } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import I18n from '../../i18n';
import styles from './styles';
import { navigateTo, logout } from '../../actions';

class ProfilePopupMenu extends Component {
  handleSettingsPress = () => {
    const { onClose, database } = this.props;
    onClose();
    const { settingsUrl } = database;
    Linking.canOpenURL(settingsUrl).then((supported) => {
      if (supported) {
        Linking.openURL(settingsUrl);
      } else {
        console.log(`Don't know how to open URI: ${settingsUrl}`);
      }
    });
  }

  handleLogoutPress = () => {
    const { navigateTo, logout, navigation } = this.props;
    this.props.onClose();
    logout();
    navigation.dispatch(navigateTo('Login'));
  }

  handleObservationPress = () => {
    const { navigateTo, navigation, database } = this.props;
    this.props.onClose();
    if (database.game === 'on') {
      navigation.dispatch(navigateTo('ObservationTabScreen'));
    } else {
      navigation.dispatch(navigateTo('LastObservationsScreen'));
    }
  }

  handleDatabasePress = () => {
    const { navigateTo, navigation, server } = this.props;
    this.props.onClose();
    navigation.dispatch(navigateTo('LastDatabasesScreen', { title: server.name }));
  }

  render() {
    const {
      name,
      observation,
      visible,
      onClose,
      showNavigationOptions,
      database,
    } = this.props;
    return (
      <Modal animationType="fade" presentationStyle="overFullScreen" transparent visible={visible} onRequestClose={onClose}>
        <View style={styles.container}>
          <TouchableWithoutFeedback onPress={onClose}>
            <View style={styles.closeArea} />
          </TouchableWithoutFeedback>
          <View style={styles.menuContainer}>
            <View style={styles.header}>
              <Text style={styles.nameText}>{name}</Text>
              {observation && <Text>{observation.name}</Text>}
            </View>
            { showNavigationOptions &&
            <TouchableOpacity onPress={this.handleObservationPress}>
              <View style={styles.row}>
                <Icon name="pets" size={20} />
                <Text style={styles.menuItemText}>{I18n.t('observation_forms')}</Text>
              </View>
            </TouchableOpacity>}
            { showNavigationOptions &&
            <TouchableOpacity onPress={this.handleDatabasePress}>
              <View style={styles.row}>
                <Icon name="format-list-numbered" size={20} />
                <Text style={styles.menuItemText}>{I18n.t('databases')}</Text>
              </View>
            </TouchableOpacity>}
            { database && database.settingsUrl &&
            <TouchableOpacity onPress={this.handleSettingsPress}>
              <View style={styles.row}>
                <Icon name="settings" size={20} />
                <Text style={styles.menuItemText}>{I18n.t('settings')}</Text>
              </View>
            </TouchableOpacity>}
            <TouchableOpacity onPress={this.handleLogoutPress}>
              <View style={styles.row}>
                <Icon name="input" size={20} />
                <Text style={styles.menuItemText}>{I18n.t('logout')}</Text>
              </View>
            </TouchableOpacity>
          </View>
        </View>
      </Modal>
    );
  }
}

ProfilePopupMenu.propTypes = {
  name: PropTypes.string.isRequired,
  server: PropTypes.shape({}).isRequired,
  database: PropTypes.shape({}),
  observation: PropTypes.shape({}),
  visible: PropTypes.bool,
  onClose: PropTypes.func.isRequired,
  showNavigationOptions: PropTypes.bool,
  navigateTo: PropTypes.func.isRequired,
  logout: PropTypes.func.isRequired,
  navigation: PropTypes.shape({}).isRequired,
};

ProfilePopupMenu.defaultProps = {
  database: {},
  observation: {},
  visible: false,
  showNavigationOptions: false,
};

const mapStateToProps = ({ server, databases, user: { username } }) => {
  const findSelected = x => x.isSelected;

  const database =
    databases && databases.find(findSelected);

  const observation =
    database && database.observations && database.observations.find(findSelected);

  return ({
    server,
    database,
    observation,
    name: username,
  });
};

export default connect(mapStateToProps, {
  navigateTo,
  logout,
})(ProfilePopupMenu);
