import React from 'react';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { footerStyle } from '..';

const getIconName = ({ netStatus }) => (netStatus === 'wifi' ? 'cloud' : 'cloud-off');

const SignalIcon = props =>
  <Icon name={getIconName(props)} size={24} color="white" style={footerStyle.button} />;

const mapStateToProps = ({ network }) => ({
  netStatus: network.status ? network.status : 'none',
});

export default connect(mapStateToProps)(SignalIcon);
