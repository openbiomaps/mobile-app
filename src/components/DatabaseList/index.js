import React, { Component } from 'react';
import { View } from 'react-native';
import PropTypes from 'prop-types';
import { SelectorList } from '../';
import Menu from '../ProfilePopupMenu';
import EmptyList from '../EmptyList';


class DatabaseList extends Component {
  state = {
    showMenu: false,
  }

  componentDidMount() {
    const { server, navigation } = this.props;
    this.onNavigationFocus = navigation.addListener('willFocus', () => {
      this.loadDatabases();
    });
    navigation.dangerouslyGetParent().setParams({
      toggleMenu: this.toggleMenu,
      title: server.name,
    });
  }

  componentWillUnmount() {
    this.onNavigationFocus.remove();
  }

  onDatabasePress = ({ id }) => {
    const {
      navigation,
      navigateTo,
      selectDatabase,
      databases,
    } = this.props;
    selectDatabase(id);
    const database = databases.find(database => database.id === id);
    if (database.game === 'on') {
      navigation.dispatch(navigateTo('ObservationTabScreen'));
    } else {
      const lastObservations = database.observations ?
        database.observations.filter(observation => observation.lastSelected) : [];
      if (lastObservations.length) {
        navigation.dispatch(navigateTo('LastObservationsScreen'));
      } else {
        navigation.dispatch(navigateTo('ObservationScreen'));
      }
    }
  }

  loadDatabases = () => {
    const { accessToken, server } = this.props;
    this.props.loadDatabases(accessToken, server.id);
  }

  toggleMenu = () => {
    this.setState(prevState => ({ showMenu: !prevState.showMenu }));
  }

  render() {
    return (
      <View style={{ flex: 1 }}>
        <Menu
          navigation={this.props.navigation}
          visible={this.state.showMenu}
          onClose={this.toggleMenu}
        />
        {this.props.databases && this.props.databases.length ?
          <SelectorList
            data={this.props.databases}
            onPress={id => this.onDatabasePress(id)}
          /> :
          <EmptyList onRefreshPress={() => this.loadDatabases()} />
        }
      </View>
    );
  }
}

DatabaseList.defaultProps = {
  accessToken: null,
  databases: [],
};

DatabaseList.propTypes = {
  accessToken: PropTypes.string,
  server: PropTypes.shape({}).isRequired,
  databases: PropTypes.arrayOf(PropTypes.shape({})),
  navigation: PropTypes.shape({ setParams: PropTypes.func.isRequired }).isRequired,
  navigateTo: PropTypes.func.isRequired,
  loadDatabases: PropTypes.func.isRequired,
  selectDatabase: PropTypes.func.isRequired,
};

export default DatabaseList;
