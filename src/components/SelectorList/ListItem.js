import React from 'react';
import { TouchableOpacity, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import styles from './styles';

export default function ListItem(props) {
  const {
    id,
    name,
    description,
    disabled,
    onPress,
  } = props;
  return (
    <TouchableOpacity
      disabled={disabled}
      onPress={() => onPress({ id, name, description })}
    >
      <View style={[styles.listItem, disabled && { opacity: 0.5 }]}>
        <Text style={styles.mainText}>{name}</Text>
        {description && <Text style={styles.subText} ellipsizeMode="tail" numberOfLines={2}>{description}</Text>}
      </View>
    </TouchableOpacity>
  );
}

ListItem.propTypes = {
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  disabled: PropTypes.bool,
  onPress: PropTypes.func,
};

ListItem.defaultProps = {
  description: null,
  disabled: false,
  onPress: null,
};
