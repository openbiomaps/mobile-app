import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  listItem: {
    paddingLeft: 25,
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    paddingVertical: 20,
    justifyContent: 'center',
  },
  mainText: {
    fontSize: 16,
    color: '#212121',
  },
  subText: {
    fontSize: 14,
    color: '#888',
  },
});
