import React from 'react';
import { FlatList } from 'react-native';
import PropTypes from 'prop-types';
import ListItem from './ListItem';

const keyExtractor = item => String(item.id);

function SelectorList({ data, onPress }) {
  return (
    <FlatList
      data={data}
      renderItem={({ item }) => <ListItem {...item} onPress={onPress} />}
      keyExtractor={keyExtractor}
    />
  );
}

SelectorList.propTypes = {
  data: PropTypes.arrayOf(PropTypes.shape(ListItem.propTypes)).isRequired,
  onPress: PropTypes.func.isRequired,
};

export { SelectorList };

