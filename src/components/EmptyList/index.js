import React, { Fragment, Component } from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
import I18n from '../../i18n';
import styles from './styles';

class EmptyList extends Component {
  static propTypes = {
    netStatus: PropTypes.string.isRequired,
    onRefreshPress: PropTypes.func.isRequired,
  }

  componentDidUpdate(prevProps) {
    const isOffline = status => status === 'unknown' || status === 'none';
    if (isOffline(prevProps.netStatus) && !isOffline(this.props.netStatus)) {
      this.props.onRefreshPress();
    }
  }

  render() {
    const { netStatus } = this.props;
    return (
      <View style={styles.container}>
        {netStatus && netStatus !== 'unknown' && netStatus !== 'none' ?
          <Text style={styles.text}>{I18n.t('empty_list')}</Text> :
          <Fragment>
            <Icon name="cloud-off" size={100} />
            <Text style={styles.text}>{I18n.t('empty_list')}</Text>
            <Text style={styles.text}>{I18n.t('needs_net')}</Text>
          </Fragment>
        }
      </View>
    );
  }
}

const mapStateToProps = ({ network: { status } }) => ({
  netStatus: status,
});

export default connect(mapStateToProps)(EmptyList);
