import React from 'react';
import { FlatList, Text, View } from 'react-native';
import PropTypes from 'prop-types';
import moment from 'moment';
import I18n from '../../i18n';
import ListItem from './ListItem';
import ImageList from './ImageList';
import styles from './styles';

const parseValue = (value, dataType) => {
  switch (dataType.type) {
    case 'time':
      return moment(value).format('HH:mm');
    case 'datetime':
      return moment(value).format('YYYY.MM.DD HH:mm');
    case 'date':
      return moment(value).format('YYYY.MM.DD');
    case 'list':
      // temporary solution for invalid list items
      return Array.isArray(value) || !Object.keys(dataType.list).find(item =>
        dataType.list[item] === value) ? value :
        Object.keys(dataType.list).find(item => dataType.list[item] === value).match(/^[^#]+/)[0];
    case 'boolean':
      return value ? I18n.t('bool_true') : I18n.t('bool_false');
    default:
      return value;
  }
};

const MeasurementInfo = ({ dataTypes, values, observation }) => {
  const data = dataTypes
    .filter(type => values[type.column] && type.type !== 'point' && type.type !== 'wkt')
    .map((data) => {
      const value = values[data.column];
      const parsedValue = parseValue(value, data);
      return {
        name: data.shortName,
        id: data.column,
        value: parsedValue,
        type: data.type,
      };
    });
  return (
    <View>
      <Text style={styles.title}>{observation}</Text>
      <FlatList
        listKey={() => String(values.id)}
        data={data}
        renderItem={({ item }) => (item.type === 'file_id' ?
          <ImageList name={item.name} data={item.value} /> :
          <ListItem name={item.name} value={item.value} />)}
        keyExtractor={item => item.id}
      />
    </View>
  );
};

MeasurementInfo.propTypes = {
  dataTypes: PropTypes.array.isRequired,
  values: PropTypes.shape({}).isRequired,
  observation: PropTypes.string.isRequired,
};

export default MeasurementInfo;
