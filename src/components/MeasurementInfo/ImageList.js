import React from 'react';
import { FlatList, View, Text } from 'react-native';
import PropTypes from 'prop-types';
import ListItem from './ImageListItem';

const ImageList = ({ name, data }) => (
  <View>
    <Text>{name}</Text>
    <FlatList
      listKey={String(name)}
      data={data}
      keyExtractor={item => String(item.id)}
      renderItem={({ item }) => (
        <ListItem uri={item.uri} />
      )}
    />
  </View>
);

ImageList.propTypes = {
  name: PropTypes.string.isRequired,
  data: PropTypes.arrayOf(PropTypes.shape({
    id: PropTypes.number.isRequired,
    uri: PropTypes.string.isRequired,
  })).isRequired,
};

export default ImageList;
