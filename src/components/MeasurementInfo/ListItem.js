import React from 'react';
import { Text } from 'react-native';
import PropTypes from 'prop-types';

const ListItem = ({ name, value }) => <Text>{name}: {value}</Text>;

ListItem.propTypes = {
  name: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
};

export default ListItem;
