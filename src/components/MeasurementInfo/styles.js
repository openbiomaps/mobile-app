import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  title: {
    fontSize: 18,
    color: 'black',
    marginBottom: 12,
  },
  imageContainer: {
    height: 200,
    width: '100%',
    marginVertical: 5,
  },
  image: {
    borderRadius: 10,
    position: 'absolute',
    top: 0,
    right: 0,
    left: 0,
    bottom: 0,
  },
});
