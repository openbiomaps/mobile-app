import { StyleSheet } from 'react-native';

export const headerStyle = {
  backgroundColor: '#7EC667',
  height: 80,
};

export const titleStyle = {
  fontWeight: '400',
};

export const headerTitleStyle = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    paddingHorizontal: 20,
    ...headerStyle,
  },
  title: {
    ...titleStyle,
    fontSize: 22,
    marginBottom: 10,
    color: 'rgba(0, 0, 0, 0.87)',
  },
  description: {
    fontSize: 12,
    color: 'rgba(0, 0, 0, 0.87)',
  },
  button: {
    fontSize: 22,
    marginLeft: 20,
  },
  centerAligned: {
    alignItems: 'center',
  },
});
