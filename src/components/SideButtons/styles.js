import { StyleSheet } from 'react-native';

const icon = {
  transform: [{ rotate: '-45deg' }],
};

const button = {
  margin: 15,
  padding: 10,
  transform: [{ rotate: '45deg' }],
  borderRadius: 10,
  elevation: 3,
};

export const sideButtonStyle = StyleSheet.create({
  buttonContainer: {
    position: 'absolute',
    bottom: 20,
    right: 10,
  },
  doneIcon: {
    ...icon,
    color: 'white',
  },
  doneButton: {
    ...button,
    backgroundColor: '#434A54',
  },
});
