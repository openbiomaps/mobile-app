import React from 'react';
import { Marker, Polyline, Polygon } from 'react-native-maps';
import Icon from 'react-native-vector-icons/MaterialIcons';

export default ({
  type,
  data,
  holes,
}) => {
  switch (type) {
    case 'point':
      return (
        <Marker
          coordinate={data}
          anchor={{ x: 0.5, y: 0.5 }}
        >
          <Icon name="photo-camera" size={15} color="#7EC667" />
        </Marker>
      );
    case 'line':
      return (
        <Polyline
          coordinates={data}
          strokeColor="white"
          strokeWidth={3}
        />
      );
    case 'area':
      return (
        <Polygon
          coordinates={data}
          holes={holes}
          fillColor="rgba(255,0,0,0.5)"
          strokeWidth={0}
        />
      );
    default:
      return null;
  }
};
