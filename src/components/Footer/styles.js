import { StyleSheet } from 'react-native';

export const footerStyle = StyleSheet.create({
  container: {
    flexDirection: 'row',
    width: '100%',
    backgroundColor: '#7EC667',
    alignItems: 'center',
    height: 44,
  },
  button: {
    paddingVertical: 10,
    paddingHorizontal: 10,
    borderRadius: 24,
  },
});
