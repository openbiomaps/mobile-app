import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  circle: {
    backgroundColor: 'white',
    borderColor: '#7EC667',
    borderWidth: 2,
    borderRadius: 7,
    height: 15,
    width: 15,
  },
  labelText: {
    backgroundColor: 'rgba(0,0,0,.3)',
    color: 'white',
    fontSize: 10,
    paddingHorizontal: 3,
    borderRadius: 3,
  },
  marker: {
    alignItems: 'center',
  },
  hint: {
    borderRadius: 4,
    fontSize: 12,
    paddingHorizontal: 10,
    paddingVertical: 5,
    backgroundColor: '#7EC667',
    color: 'white',
    textAlign: 'center',
  },
});
