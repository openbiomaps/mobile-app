import React from 'react';
import PropTypes from 'prop-types';
import { Text, StyleSheet, View } from 'react-native';
import MapView, { Marker } from 'react-native-maps';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PermanentSamplePlot from '../PermanentSamplePlot';
import I18n from '../../i18n';
import styles from './styles';

const MapComponent = ({
  permanentSamplePlots,
  labels,
  lastMeasurements,
  region,
  onRegionChange,
  onMeasurementPress,
  position,
  mapRef,
  onLongPress,
  inputPosition,
  inputRef,
  showInputHint,
  liteMode,
  style,
}) => (
  <MapView
    ref={mapRef}
    mapType="hybrid"
    region={region}
    onRegionChangeComplete={onRegionChange}
    style={[StyleSheet.absoluteFillObject, style]}
    pitchEnabled={false}
    onLongPress={onLongPress}
    liteMode={liteMode}
    cacheEnabled
  >
    {position && (
      <Marker coordinate={position} anchor={{ x: 0.5, y: 0.9 }}>
        <Icon name="person-pin" color="white" size={30} />
      </Marker>
    )}
    {lastMeasurements && lastMeasurements.map(measurement => (
      <Marker
        coordinate={measurement.coordinate}
        onPress={onMeasurementPress && (() => onMeasurementPress(measurement))}
        key={measurement.id}
        anchor={{ x: 0.5, y: 0.5 }}
      >
        <View style={styles.circle} />
      </Marker>
    ))}
    {permanentSamplePlots && permanentSamplePlots.map(plot => (
      <PermanentSamplePlot
        key={plot.id}
        {...plot}
      />
    ))}
    {labels && labels.map(label => (
      <Marker
        coordinate={label.coordinate}
        key={`label-${label.id}`}
      >
        <Text style={styles.labelText}>{label.name}</Text>
      </Marker>
    ))}
    {inputPosition && (
      <Marker
        coordinate={inputPosition}
        anchor={{ x: 0.5, y: 0.9 }}
        ref={inputRef}
      >
        <View style={styles.marker}>
          {showInputHint &&
          <Text style={styles.hint}>{I18n.t('position_input_hint')}</Text>}
          <Icon name="place" size={40} color="white" />
        </View>
      </Marker>
    )}
  </MapView>
);

MapComponent.propTypes = {
  permanentSamplePlots: PropTypes.arrayOf(PropTypes.shape({})),
  labels: PropTypes.arrayOf(PropTypes.shape({})),
  lastMeasurements: PropTypes.arrayOf(PropTypes.shape({})),
  region: PropTypes.shape({}).isRequired,
  onRegionChange: PropTypes.func,
  onMeasurementPress: PropTypes.func,
  position: PropTypes.shape({}),
  mapRef: PropTypes.func,
  onLongPress: PropTypes.func,
  inputPosition: PropTypes.shape({}),
  inputRef: PropTypes.func,
  showInputHint: PropTypes.bool,
  liteMode: PropTypes.bool,
  style: PropTypes.shape({}),
};

MapComponent.defaultProps = {
  permanentSamplePlots: null,
  onRegionChange: null,
  onMeasurementPress: null,
  position: null,
  labels: null,
  lastMeasurements: null,
  liteMode: false,
  style: null,
  mapRef: null,
  onLongPress: null,
  inputPosition: null,
  inputRef: null,
  showInputHint: false,
};

export default MapComponent;
