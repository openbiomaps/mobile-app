import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    justifyContent: 'flex-end',
    height: '100%',
    backgroundColor: 'rgba(20,20,20,.3)',
  },
  closeArea: { flex: 1 },
  popup: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    backgroundColor: 'white',
    borderTopRightRadius: 20,
    borderTopLeftRadius: 20,
    paddingTop: 25,
    paddingBottom: 25,
    paddingHorizontal: 15,
    marginHorizontal: 10,
  },
});
