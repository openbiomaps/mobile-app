import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { SelectorList } from '../';
import { resetStack } from '../../actions';
import EmptyList from '../EmptyList';

class ObservationList extends Component {
  componentDidMount() {
    const {
      database,
      navigation,
    } = this.props;

    this.onNavigationFocus = navigation.addListener('willFocus', () => {
      this.loadObservations();
    });

    navigation.dangerouslyGetParent().setParams({
      title: database.name,
    });
  }

  componentWillUnmount() {
    this.onNavigationFocus.remove();
  }

  onObservationPress = ({ id }) => {
    const {
      database,
      observations,
      navigation,
      navigateTo,
      selectObservation,
    } = this.props;
    const act = observations.find(observation => observation.id === id);
    selectObservation(database.id, id);
    if (act.game && !act.game.passedQuiz) {
      navigation.dispatch(resetStack('QuizInformationScreen', { title: act.game.title }));
    } else {
      navigation.dispatch(navigateTo('ObservationMapScreen'));
    }
  }

  loadObservations = () => {
    const {
      accessToken,
      database,
    } = this.props;
    this.props.loadObservations(database, accessToken);
  }

  render() {
    const { observations } = this.props;
    return (
      observations && observations.length ?
        <SelectorList
          data={observations}
          onPress={id => this.onObservationPress(id)}
        /> :
        <EmptyList onRefreshPress={() => this.loadObservations()} />
    );
  }
}

ObservationList.defaultProps = {
  accessToken: null,
  observations: [],
  database: {
    observations: [],
  },
};

ObservationList.propTypes = {
  accessToken: PropTypes.string,
  database: PropTypes.shape({}),
  observations: PropTypes.arrayOf(PropTypes.shape({})),
  navigation: PropTypes.shape({}).isRequired,
  navigateTo: PropTypes.func.isRequired,
  loadObservations: PropTypes.func.isRequired,
  selectObservation: PropTypes.func.isRequired,
};

export default ObservationList;
