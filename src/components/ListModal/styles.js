import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  modal: {
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    backgroundColor: 'rgba(0,0,0,0.4)',
  },
  content: {
    flex: 1,
    marginHorizontal: 30,
    marginVertical: 80,
    borderRadius: 5,
    padding: 0,
    backgroundColor: '#fff',
    elevation: 1,
  },
});
