import React from 'react';
import {
  View,
  Modal,
} from 'react-native';
import PropTypes from 'prop-types';
import { SelectorList } from '../index';
import styles from './styles';

const ListModal = ({
  isOpen,
  listData,
  onSelect,
}) => (
  <Modal
    transparent
    visible={isOpen}
    onRequestClose={() => {
        console.log('onRequestClose');
      }}
  >
    <View style={styles.modal}>
      <View style={styles.content}>
        <SelectorList
          data={listData || []}
          onPress={props => onSelect(props)}
        />
      </View>
    </View>
  </Modal>
);

ListModal.propTypes = {
  isOpen: PropTypes.bool,
  listData: PropTypes.arrayOf(PropTypes.shape({})),
  onSelect: PropTypes.func,
};

ListModal.defaultProps = {
  isOpen: false,
  listData: [],
  onSelect: () => ({}),
};

export { ListModal };

