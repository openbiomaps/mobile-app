import React, { Component } from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import I18n from '../../i18n';
import { createMeasurement } from '../../actions';
import { headerTitleStyle, headerStyle, MeasurementForm } from '../../components';

class CreateMeasurementScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params } } = navigation;
    return {
      headerTitle: (
        <View style={headerTitleStyle.container}>
          <Text style={headerTitleStyle.title}>{params ? params.title : ''}</Text>
          <Text style={headerTitleStyle.description}>{I18n.t('form')}</Text>
        </View>
      ),
      headerStyle,
    };
  };

  static propTypes = {
    id: PropTypes.number,
    observation: PropTypes.shape({}).isRequired,
    database: PropTypes.shape({}).isRequired,
    navigation: PropTypes.shape({}).isRequired,
    createMeasurement: PropTypes.func.isRequired,
  }

  static defaultProps = {
    id: 0,
  }

  handleDonePress = (measurement) => {
    const {
      database,
      observation,
      createMeasurement,
      navigation,
    } = this.props;
    createMeasurement(database.id, observation.id, measurement);
    const onSave = navigation.getParam('onSave', () => {});
    onSave(I18n.t('measurement_created_message'));
  }

  render() {
    return (
      <MeasurementForm
        id={this.props.id}
        onDonePress={this.handleDonePress}
        navigation={this.props.navigation}
      />
    );
  }
}

const mapStateToProps = ({ databases }) => {
  const database = databases.find(database => database.isSelected);
  const observation = database && database.observations.find(observation => observation.isSelected);
  const measurements = observation && observation.measurements ? [...observation.measurements] : [];
  const id = (measurements && measurements.length > 0) ?
    Math.max(...observation.measurements.map(item => (item.id || 0))) + 1 : 0;
  return ({
    database,
    observation,
    id: id || 0,
  });
};


export default connect(
  mapStateToProps,
  { createMeasurement },
)(CreateMeasurementScreen);
