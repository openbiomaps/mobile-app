import { connect } from 'react-redux';
import { loadDatabases, selectDatabase, navigateTo } from '../../actions';
import DatabaseList from '../../components/DatabaseList';

const mapStateToProps = ({ user: { accessToken }, server, databases }) => ({
  accessToken,
  server,
  databases,
});

export default connect(
  mapStateToProps,
  { loadDatabases, selectDatabase, navigateTo },
)(DatabaseList);
