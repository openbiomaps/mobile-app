import React, { Component } from 'react';
import { View, Text, TouchableWithoutFeedback, ActivityIndicator } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Geolocation from 'react-native-geolocation-service';
import I18n from '../../i18n';
import styles from './styles';
import MapComponent from '../../components/Map';
import { footerStyle } from '../../components';
import { setParams, navigateBack } from '../../actions';
import { getCenterOfPlot } from '../../utils';

class PositionInput extends Component {
  static navigationOptions = {
    header: null,
  }

  static propTypes = {
    navigation: PropTypes.shape({ getParam: PropTypes.func.isRequired }).isRequired,
    observation: PropTypes.shape({}).isRequired,
  };

  constructor(props) {
    super(props);
    const position = props.navigation.getParam('position', null);

    const { observation, navigation } = this.props;
    const column = navigation.getParam('column');
    const { form: { dataTypes } } = observation;
    const field = dataTypes.find(field => field.column === column);
    const permanentSamplePlots = field.permanentSamplePlots || [];
    const labels = permanentSamplePlots.map(({
      name,
      data,
      type,
      id,
    }) => ({
      name,
      id,
      coordinate: getCenterOfPlot(type, data),
    }));

    this.state = position ? {
      region: {
        latitude: position.latitude,
        longitude: position.longitude,
        latitudeDelta: 0.0082,
        longitudeDelta: 0.0051,
      },
      markerPosition: position,
      showHint: true,
      buttonIndicator: false,
      indicator: false,
      permanentSamplePlots,
      labels,
    } : {
      showHint: true,
      indicator: true,
      buttonIndicator: false,
      permanentSamplePlots,
      labels,
    };
  }

  componentDidMount = () => {
    const position = this.props.navigation.getParam('position', null);
    if (!position) {
      this.setState({
        error: I18n.t('gps_timeout'),
      });
      this.getInitialPosition();
    }
  }

  getInitialPosition = () => {
    const deltas = {
      latitudeDelta: 0.0082,
      longitudeDelta: 0.0051,
    };
    Geolocation.getCurrentPosition(
      ({ coords }) => {
        const { longitude, latitude } = coords;
        const coordinate = { longitude, latitude };
        const region = {
          ...deltas,
          ...coordinate,
        };
        this.setState({
          region,
          markerPosition: coordinate,
          error: null,
          indicator: false,
        });
      },
      () => {
        // fallback position if gps is unavailable
        const position = this.getFallbackPosition();
        this.setState({
          error: I18n.t('gps_unavailable'),
          indicator: false,
          region: {
            ...position,
            ...deltas,
          },
          markerPosition: position,
        });
      },
      {
        timeout: 15000,
        enableHighAccuracy: true,
        maximumAge: 20000,
      },
    );
  }

  getFallbackPosition = () => {
    const [firstPlot] = this.state.permanentSamplePlots;

    const fallbackPosition = firstPlot ?
      getCenterOfPlot(firstPlot.type, firstPlot.data) :
      // Budapest if there are no permanent sample plots for the form
      { latitude: 47.4979, longitude: 19.0402 };
    return fallbackPosition;
  }

  handleRegionChange = (region) => {
    this.setState({ region });
  }

  handlePress = async (e) => {
    this.hideHint();
    const { coordinate } = e.nativeEvent;
    this.marker.animateMarkerToCoordinate(coordinate, 300);
    setTimeout(() => {
      this.setState({
        markerPosition: coordinate,
      });
    }, 300);
  }

  hideHint = () => {
    this.setState({
      showHint: false,
    });
  }

  handleMyLocationPress = () => {
    this.hideHint();
    this.setState({ buttonIndicator: true });
    Geolocation.getCurrentPosition(
      ({ coords }) => {
        this.setState({
          markerPosition: coords,
          showHint: false,
          error: null,
          buttonIndicator: false,
        }, () => {
          this.map.animateToCoordinate(coords);
        });
      },
      () => this.setState({ error: I18n.t('gps_unavailable'), buttonIndicator: false }),
      {
        timeout: 15000,
        maximumAge: 20000,
      },
    );
  }

  handleDonePress = () => {
    const { navigation } = this.props;
    const key = navigation.getParam('key');
    const column = navigation.getParam('column');

    navigation.dispatch(navigateBack());
    navigation.dispatch(setParams({
      positionInputCoordinates: { [column]: this.state.markerPosition },
    }, key));
  }

  handleBackPress = () => {
    const { navigation } = this.props;
    navigation.dispatch(navigateBack());
  }

  render() {
    const { permanentSamplePlots, labels } = this.state;
    return (
      <View style={styles.container}>
        {this.state.indicator ?
          <ActivityIndicator style={styles.spinner} /> :
          <MapComponent
            permanentSamplePlots={permanentSamplePlots}
            labels={labels}
            region={this.state.region}
            onRegionChange={this.handleRegionChange}
            mapRef={(ref) => { this.map = ref; }}
            onLongPress={this.handlePress}
            inputRef={(ref) => { this.marker = ref; }}
            inputPosition={this.state.markerPosition}
            showInputHint={this.state.showHint}
          />
        }
        <View style={styles.buttonContainer}>
          <TouchableWithoutFeedback onPress={this.handleMyLocationPress}>
            <View style={styles.myLocationButton}>
              {this.state.buttonIndicator ?
                <ActivityIndicator style={styles.spinner} /> :
                <Icon name="my-location" size={24} style={styles.myLocationIcon} />
              }
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={this.handleDonePress}>
            <View style={styles.doneButton}>
              <Icon name="done" size={24} style={styles.doneIcon} />
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={footerStyle.container}>
          <TouchableWithoutFeedback onPress={this.handleBackPress}>
            <Icon name="close" size={24} color="black" style={footerStyle.button} />
          </TouchableWithoutFeedback>
        </View>
        {this.state.error &&
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>{this.state.error}</Text>
          </View>}
      </View>
    );
  }
}

const mapStateToProps = ({ databases }) => {
  const database = databases.find(database => database.isSelected);
  const observation = database.observations.find(observation => observation.isSelected);
  return { observation };
};

export default connect(mapStateToProps)(PositionInput);
