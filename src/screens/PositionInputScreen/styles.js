import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  buttonContainer: {
    position: 'absolute',
    bottom: 20,
    right: 10,
  },
  myLocationIcon: {
    transform: [{ rotate: '-45deg' }],
    color: '#333',
  },
  myLocationButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 2,
  },
  doneIcon: {
    transform: [{ rotate: '-45deg' }],
    color: 'white',
  },
  doneButton: {
    backgroundColor: '#434A54',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 3,
  },
  container: {
    flex: 1,
    width: '100%',
    justifyContent: 'flex-end',
  },
  errorContainer: {
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.87)',
    top: 0,
    left: 0,
    right: 0,
    padding: 10,
  },
  errorText: {
    color: 'white',
  },
  spinner: {
    marginVertical: 2,
  },
});
