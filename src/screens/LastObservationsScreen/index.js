import { connect } from 'react-redux';
import ObservationList from '../../components/ObservationList';
import { loadObservations, selectObservation, navigateTo } from '../../actions';

const mapStateToProps = ({ user: { accessToken }, databases }) => {
  const selectedDatabase = databases.find(database => database.isSelected);
  const observations = (selectedDatabase && selectedDatabase.observations) ?
    selectedDatabase.observations.filter(observation => observation.lastSelected) : [];
  observations.sort((a, b) => new Date(b.lastSelected) - new Date(a.lastSelected));
  return ({
    accessToken,
    observations,
    database: selectedDatabase,
  });
};


export default connect(
  mapStateToProps,
  { loadObservations, selectObservation, navigateTo },
)(ObservationList);

