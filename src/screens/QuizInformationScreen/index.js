import React, { Component, Fragment } from 'react';
import { ScrollView, Text, TouchableHighlight, TouchableWithoutFeedback, View } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import HTMLView from 'react-native-htmlview';
import { navigateTo } from '../../actions';
import { headerTitleStyle, titleStyle, headerStyle, footerStyle, sideButtonStyle } from '../../components';
import Menu from '../../components/ProfilePopupMenu';
import styles from './styles';
import { renderNode } from '../../utils';

class QuizInformationScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params } } = navigation;
    return {
      headerTitle: (
        <View style={[headerTitleStyle.container, headerTitleStyle.centerAligned]}>
          <Text style={headerTitleStyle.title} numberOfLines={1} >{params ? `${params.title}` : ''}</Text>
        </View>
      ),
      headerStyle,
      headerTitleStyle: titleStyle,
    };
  };

  static propTypes = {
    navigation: PropTypes.shape({}).isRequired,
    navigateTo: PropTypes.func.isRequired,
    content: PropTypes.string.isRequired,
  };

  state = {
    showMenu: false,
  }

  toggleMenu = () => {
    this.setState(prevState => ({ showMenu: !prevState.showMenu }));
  }
  handleDonePress = () => {
    const { navigation, navigateTo } = this.props;
    const { state: { params } } = navigation;
    navigation.dispatch(navigateTo('QuizQuestionsScreen', { title: params ? params.title : '' }));
  }

  render() {
    return (
      <Fragment>
        <ScrollView contentContainerStyle={styles.container} removeClippedSubviews>
          <Menu
            showNavigationOptions
            navigation={this.props.navigation}
            visible={this.state.showMenu}
            onClose={this.toggleMenu}
          />
          <HTMLView value={this.props.content} stylesheet={styles} renderNode={renderNode} />
        </ScrollView>
        <View style={sideButtonStyle.buttonContainer}>
          <TouchableWithoutFeedback onPress={this.handleDonePress}>
            <View style={sideButtonStyle.doneButton}>
              <Icon name="done" size={24} style={sideButtonStyle.doneIcon} />
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={footerStyle.container}>
          <TouchableHighlight onPress={this.toggleMenu} underlayColor="rgba(0,0,0,.2)">
            <View>
              <Icon name="menu" size={24} color="black" style={footerStyle.button} />
            </View>
          </TouchableHighlight>
        </View>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ databases }) => {
  const database = databases.find(database => database.isSelected);
  const observation = database.observations.find(observation => observation.isSelected);
  const content = observation && observation.game && observation.game.html;
  return { content };
};

export default connect(
  mapStateToProps,
  { navigateTo },
)(QuizInformationScreen);
