import React, { Component } from 'react';
import { View, Text } from 'react-native';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import I18n from '../../i18n';
import { updateMeasurement } from '../../actions';
import { headerTitleStyle, headerStyle, MeasurementForm } from '../../components';

class EditMeasurementScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params } } = navigation;
    return {
      headerTitle: (
        <View style={headerTitleStyle.container}>
          <Text style={headerTitleStyle.title}>{params ? params.title : ''}</Text>
          <Text style={headerTitleStyle.description}>{I18n.t('form')}</Text>
        </View>
      ),
      headerStyle,
    };
  };

  static propTypes = {
    observation: PropTypes.shape({}).isRequired,
    database: PropTypes.shape({}).isRequired,
    navigation: PropTypes.shape({}).isRequired,
    updateMeasurement: PropTypes.func.isRequired,
  }

  handleDonePress = (measurement) => {
    const {
      database,
      observation,
      navigation,
      updateMeasurement,
    } = this.props;
    const { state: { params } } = navigation;
    updateMeasurement(database.id, observation.id, params.measurement.id, measurement);
    const onSave = navigation.getParam('onSave', () => {});
    onSave(I18n.t('measurement_updated_message'));
  }

  render() {
    const { navigation: { state: { params } } } = this.props;
    const { measurement } = params;
    return (
      <MeasurementForm
        id={measurement.id}
        onDonePress={this.handleDonePress}
        navigation={this.props.navigation}
        values={measurement.data}
        errors={measurement.errors || {}}
      />
    );
  }
}

const mapStateToProps = ({ databases }) => {
  const database = databases.find(database => database.isSelected);
  const observation = database && database.observations.find(observation => observation.isSelected);
  return ({
    database,
    observation,
  });
};


export default connect(
  mapStateToProps,
  { updateMeasurement },
)(EditMeasurementScreen);
