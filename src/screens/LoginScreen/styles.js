import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  background: {
    width: '100%',
    height: '100%',
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  container: {
    flex: 1,
    width: '100%',
    alignItems: 'center',
    justifyContent: 'flex-end',
    paddingHorizontal: 30,
  },
  inputFieldContainer: {
    borderTopRightRadius: 4,
    borderTopLeftRadius: 4,
    marginHorizontal: 15,
    marginTop: 40,
    height: 60,
    paddingBottom: 0,
    justifyContent: 'flex-end',
    width: '100%',
    backgroundColor: 'white',
  },
  loginButton: {
    borderRadius: 8,
    backgroundColor: '#7EC667',
    borderWidth: 3,
    borderColor: '#555',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 8.5,
    paddingHorizontal: 45,
    marginRight: 15,
    elevation: 2,
  },
  row: {
    flexDirection: 'row',
    marginBottom: 10,
    marginTop: 40,
  },
  buttonText: {
    color: 'white',
    fontSize: 14,
    letterSpacing: 2,
  },
  keyboardAvoidingContainer: {
    flex: 1,
    width: '100%',
    marginBottom: 30,
  },
  passwordButton: {
    position: 'absolute',
    width: '100%',
    height: '100%',
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 15,
  },
  errorContainer: {
    marginTop: 3,
    marginHorizontal: 15,
    paddingLeft: 15,
    width: '100%',
  },
  errorText: {
    fontSize: 12,
    color: 'rgba(0, 0, 0, .8)',
  },
});
