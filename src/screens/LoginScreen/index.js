import React, { Component, Fragment } from 'react';
import {
  ImageBackground,
  View,
  KeyboardAvoidingView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  TouchableHighlight,
  Text,
  ActivityIndicator,
  Alert,
} from 'react-native';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import I18n from '../../i18n';
import styles from './styles';
import { TextInput, ListModal } from '../../components';
import { login, selectServer, navigateTo, resetStack, purgeData, startRefreshTokenListener } from '../../actions';
import client from '../../config/apiClient';
import backgroundImage from '../../assets/login_background.jpg';

const DIFFERENT_USER = 'DIFFERENT_USER';
const DIFFERENT_SERVER = 'DIFFERENT_SERVER';

class LoginScreen extends Component {
  static navigationOptions = {
    header: null,
  };

  static getDerivedStateFromProps = (newProps, prevState) => {
    const {
      server,
      accessToken,
      selectedObservation,
      navigation,
      databases,
    } = newProps;
    if (!prevState.accessToken && accessToken) {
      client.prefix = server.url;
      if (selectedObservation) {
        if (selectedObservation.game && !selectedObservation.game.passedQuiz) {
          const { game: { title } } = selectedObservation;
          navigation.dispatch(resetStack('QuizInformationScreen', { title }));
        } else {
          navigation.dispatch(navigateTo('ObservationMapScreen'));
        }
      } else if (server) {
        const lastDatabases = databases ? databases.filter(database => database.lastSelected) : [];
        if (lastDatabases.length) {
          navigation.dispatch(navigateTo('LastDatabasesScreen', { title: server.name }));
        } else {
          navigation.dispatch(navigateTo('DatabaseListScreen', { title: server.name }));
        }
      }
    }

    return { accessToken };
  }

  constructor(props) {
    super(props);
    this.state = {
      userName: '',
      password: '',
      isPasswordHidden: true,
      isServerModalOpen: false,
      servers: null,
      fetching: false,
    };
  }

  componentDidMount() {
    const {
      server,
    } = this.props;
    const url = server && server.url;
    this.props.startRefreshTokenListener();
    if (url) {
      client.prefix = url;
    }
    this.getServerList();
  }

  componentDidUpdate(prevProps, prevState) {
    if ((!prevState.servers || prevState.servers.length === 0) && !prevState.fetching) {
      this.getServerList();
    }
  }

  onServerInput = () => {
    this.setState({ isServerModalOpen: true, emptyFields: false });
  }

  onServerPress = (server) => {
    const { id, name, description } = server;
    this.setState({
      isServerModalOpen: false,
      emptyFields: false,
      server: {
        id,
        name,
        url: description,
      },
    });
  }

  getServerList = async () => {
    try {
      this.setState({ fetching: true });
      const res = await client.getServerList();
      const servers = res
        .filter(server => server.domain)
        .map(({ domain, institute }, index) => ({
          id: index,
          url: domain,
          name: institute || '',
        }));


      this.setState({
        servers,
        fetching: false,
        serverError: null,
      });
    } catch (error) {
      this.setState({
        fetching: false,
        servers: [],
        serverError: error.message,
      });
    }
  }

  selectServer = () => {
    const { server } = this.state;
    const { selectServer } = this.props;
    selectServer(this.state.server);
    client.prefix = server.url;
  }

  handleUserNameChange = (userName) => {
    this.setState({ userName, emptyFields: false });
  }

  handlePasswordChange = (password) => {
    this.setState({ password, emptyFields: false });
  }

  handleLoginPress = () => {
    const { userName, password, server } = this.state;
    if (userName !== '' && password !== '' && server) {
      const different = this.checkIfDifferentAccount();
      const isUnsynced = this.checkForUnsyncedMeasurements();
      if (different && isUnsynced) {
        const { userName: prevUserName, server: { name: prevServer } } = this.props;
        Alert.alert(
          null,
          I18n.t(
            `different_${different === DIFFERENT_SERVER ? 'server' : 'username'}`,
            { userName: prevUserName, server: prevServer },
          ),
          [
            { text: I18n.t('cancel'), style: 'cancel' },
            {
              text: I18n.t('login_anyway'),
              onPress: () => {
                this.props.purgeData();
                this.selectServer();
                this.login();
              },
            },
          ],
        );
      } else {
        if (different) {
          this.props.purgeData();
        }
        this.selectServer();
        this.login();
      }
    } else {
      this.setState({ emptyFields: true });
    }
  }

  togglePasswordVisibility = () => {
    this.setState(prevState => ({
      isPasswordHidden: !prevState.isPasswordHidden,
    }));
  }

  checkForUnsyncedMeasurements = () => {
    const { databases } = this.props;
    let isUnsynced = false;
    databases.forEach((database) => {
      if (database.observations) {
        database.observations.forEach((observation) => {
          if (observation.measurements) {
            observation.measurements.forEach((measurement) => {
              if (!measurement.isSynced || measurement.errors) {
                isUnsynced = true;
              }
            });
          }
        });
      }
    });
    return isUnsynced;
  }

  checkIfDifferentAccount = () => {
    const { userName: prevUserName, server: prevServer } = this.props;
    const { userName, server } = this.state;
    if (!prevUserName || !prevServer) {
      return false;
    }
    if (prevUserName !== userName) {
      return DIFFERENT_USER;
    }
    if (prevServer.url !== server.url) {
      return DIFFERENT_SERVER;
    }
    return false;
  }

  login = () => {
    const { userName, password } = this.state;
    this.props.login({
      username: userName,
      password,
    });
  }

  render() {
    const { server } = this.state;
    return (
      <ImageBackground source={backgroundImage} style={styles.background} resizeMode="cover">
        {
        this.state.servers &&
        <ListModal
          isOpen={this.state.isServerModalOpen}
          listData={this.state.servers.map(server => ({ ...server, description: server.url }))}
          onSelect={this.onServerPress}
        />
      }
        <KeyboardAvoidingView style={styles.keyboardAvoidingContainer}>
          <View style={styles.container}>
            {!this.state.servers ?
              <ActivityIndicator
                color="#fff"
                size="large"
              /> :
              <Fragment>
                <TouchableWithoutFeedback
                  onPress={this.onServerInput}
                >
                  <View style={styles.inputFieldContainer}>
                    <TextInput
                      label={I18n.t('server_name')}
                      editable={false}
                      value={server ? server.name : ''}
                    />
                  </View>
                </TouchableWithoutFeedback>
                <View style={styles.inputFieldContainer}>
                  <TextInput
                    label={I18n.t('username')}
                    value={this.state.userName}
                    onChangeText={this.handleUserNameChange}
                  />
                </View>
                <View style={styles.inputFieldContainer}>
                  <TextInput
                    label={I18n.t('password')}
                    value={this.state.password}
                    secureTextEntry={this.state.isPasswordHidden}
                    onChangeText={this.handlePasswordChange}
                    paddingRight={50}
                  />
                  <View style={styles.passwordButton}>
                    <TouchableOpacity onPress={this.togglePasswordVisibility}>
                      <Icon name="remove-red-eye" size={25} />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={styles.errorContainer}>
                  <Text style={styles.errorText}>{this.props.error}</Text>
                  <Text style={styles.errorText}>{this.state.serverError && I18n.t('server_error')}</Text>
                  <Text style={styles.errorText}>{this.state.emptyFields && I18n.t('empty_fields')}</Text>
                </View>
                <View style={styles.row}>
                  <TouchableHighlight underlayColor="#69a855" style={styles.loginButton} onPress={this.handleLoginPress}>
                    <Text style={styles.buttonText}>{I18n.t('login')}</Text>
                  </TouchableHighlight>
                </View>
              </Fragment>
            }
          </View>
        </KeyboardAvoidingView>
      </ImageBackground>
    );
  }
}

LoginScreen.defaultProps = {
  error: '',
  server: null,
  userName: null,
  databases: [],
};

LoginScreen.propTypes = {
  login: PropTypes.func.isRequired,
  error: PropTypes.string,
  server: PropTypes.shape({}),
  selectServer: PropTypes.func.isRequired,
  startRefreshTokenListener: PropTypes.func.isRequired,
  userName: PropTypes.string,
  purgeData: PropTypes.func.isRequired,
  databases: PropTypes.array,
};

const getSelectedObservation = (databases) => {
  if (databases && databases.length > 0) {
    const selectedDatabase = databases.find(database => database.isSelected);
    if (selectedDatabase &&
        selectedDatabase.observations &&
        selectedDatabase.observations.length > 0) {
      const selectedObservation = selectedDatabase.observations
        .find(observation => observation.isSelected);
      return selectedObservation;
    }
  }
  return false;
};

const mapStateToProps = ({
  user: {
    username: userName,
    accessToken,
    fetching,
    error,
  },
  databases,
  server,
}) => ({
  accessToken,
  fetching,
  error,
  server,
  databases,
  selectedObservation: getSelectedObservation(databases),
  userName,
});


export default connect(
  mapStateToProps,
  {
    login,
    selectServer,
    navigateTo,
    resetStack,
    purgeData,
    startRefreshTokenListener,
  },
)(LoginScreen);

