import React, { Component, Fragment } from 'react';
import { ScrollView, Text, TouchableWithoutFeedback, View } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import HTMLView from 'react-native-htmlview';
import { navigateTo } from '../../actions';
import { headerTitleStyle, titleStyle, headerStyle, footerStyle, sideButtonStyle } from '../../components';
import styles from '../QuizInformationScreen/styles';
import I18n from '../../i18n';
import { renderNode } from '../../utils';

class QuizTaskScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params } } = navigation;
    return {
      headerTitle: (
        <View style={headerTitleStyle.container}>
          <Text style={headerTitleStyle.title} numberOfLines={1} >{params ? `${params.title}` : ''}</Text>
          <Text style={headerTitleStyle.description}>{I18n.t('task_screen_title')}</Text>
        </View>
      ),
      headerStyle,
      headerTitleStyle: titleStyle,
    };
  };

  static propTypes = {
    navigation: PropTypes.shape({}).isRequired,
    navigateTo: PropTypes.func.isRequired,
    content: PropTypes.string.isRequired,
  };

  handleDonePress = () => {
    const { navigation, navigateTo } = this.props;
    navigation.dispatch(navigateTo('ObservationMapScreen'));
  }

  render() {
    return (
      <Fragment>
        <ScrollView contentContainerStyle={styles.container}>
          <HTMLView value={this.props.content} stylesheet={styles} renderNode={renderNode} />
        </ScrollView>
        <View style={sideButtonStyle.buttonContainer}>
          <TouchableWithoutFeedback onPress={this.handleDonePress}>
            <View style={sideButtonStyle.doneButton}>
              <Icon name="done" size={24} style={sideButtonStyle.doneIcon} />
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={footerStyle.container} />
      </Fragment>
    );
  }
}

const mapStateToProps = ({ databases }) => {
  const database = databases.find(database => database.isSelected);
  const observation = database.observations.find(observation => observation.isSelected);
  const content = observation.game.taskDescription;
  return { content };
};

export default connect(
  mapStateToProps,
  { navigateTo },
)(QuizTaskScreen);
