import React, { Component, Fragment } from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import ObservationCardList from '../../components/ObservationCardList';
import { navigateTo, navigateBack, editMeasurement } from '../../actions';
import { footerStyle } from '../../components';
import I18n from '../../i18n';
import { headerStyles } from './styles';
import styles from '../ObservationsMapScreen/styles';

class ObservationCardsScreen extends Component {
  static navigationOptions = ({ navigation }) => ({
    headerLeft: null,
    headerTitle: (
      <View style={headerStyles.container}>
        <View style={footerStyle.container}>
          <TouchableOpacity style={footerStyle.button} onPress={() => navigation.goBack()}>
            <View>
              <Icon name="close" size={24} color="black" />
            </View>
          </TouchableOpacity>
        </View>
        <View style={headerStyles.titleContainer}>
          <Text style={headerStyles.titleText}>{I18n.t('measurements')}</Text>
        </View>
        {navigation.state.params.message &&
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>{navigation.state.params.message}</Text>
          </View>}
      </View>
    ),
    headerStyle: headerStyles.header,
  })

  static propTypes = {
    navigation: PropTypes.shape({}).isRequired,
    navigateTo: PropTypes.func.isRequired,
    observation: PropTypes.shape({}).isRequired,
    database: PropTypes.shape({}).isRequired,
    editMeasurement: PropTypes.func.isRequired,
  };

  showMessage = (message) => {
    const { navigation } = this.props;
    navigation.setParams({ message });
    setTimeout(() => {
      navigation.setParams({ message: null });
    }, 5000);
  }

  handleMeasurementPress = (measurement) => {
    const {
      navigation,
      navigateTo,
      observation,
      database,
      editMeasurement,
    } = this.props;
    editMeasurement(database.id, observation.id, measurement.id);
    navigation.dispatch(navigateTo('EditMeasurementScreen', {
      title: observation.name,
      measurement,
      onSave: this.showMessage,
    }));
  }

  render() {
    const {
      observation: { name, form, measurements },
    } = this.props;

    return (
      <Fragment>
        <ObservationCardList
          measurements={measurements || []}
          dataTypes={(form && form.dataTypes) || []}
          observationName={name}
          onPress={this.handleMeasurementPress}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = ({ databases }) => {
  const database = databases.find(database => database.isSelected);
  const observation = database.observations.find(observation => observation.isSelected);
  return ({ database, observation });
};

export default connect(mapStateToProps, {
  navigateTo,
  navigateBack,
  editMeasurement,
})(ObservationCardsScreen);
