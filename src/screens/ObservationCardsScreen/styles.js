import { StyleSheet } from 'react-native';

export const headerStyles = StyleSheet.create({
  container: {
    backgroundColor: '#7EC667',
    width: '100%',
  },
  titleContainer: {
    backgroundColor: 'white',
    borderTopLeftRadius: 20,
    borderTopRightRadius: 20,
  },
  titleText: {
    marginTop: 15,
    marginBottom: 10,
    marginHorizontal: 25,
    fontSize: 30,
    color: 'black',
  },
  header: {
    height: 108,
  },
});
