import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#7EC667',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    paddingVertical: 20,
    paddingHorizontal: 10,
    color: 'rgba(0,0,0,.7)',
    fontSize: 18,
    fontWeight: 'bold',
    textAlign: 'center',
  },
  icon: {
    fontSize: 180,
    color: 'rgba(0,0,0,.7)',
  },
  buttonContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  button: {
    backgroundColor: 'rgb(235,255,235)',
    elevation: 2,
    borderRadius: 5,
    overflow: 'hidden',
  },
  buttonTextContainer: {
    paddingHorizontal: 50,
    paddingVertical: 10,
  },
  messageContainer: {
    flex: 2,
    justifyContent: 'flex-end',
    alignItems: 'center',
  },
});
