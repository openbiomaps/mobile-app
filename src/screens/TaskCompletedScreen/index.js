import React from 'react';
import { View, Text, TouchableNativeFeedback } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { navigateTo, resetStack } from '../../actions/navigation';
import I18n from '../../i18n';
import styles from './styles';

const TaskCompletedScreen = ({ navigation }) => (
  <View style={styles.container}>
    <View style={styles.messageContainer}>
      <Icon style={styles.icon} name="check" />
      <Text style={styles.text}>
        {I18n.t('task_completed')}
      </Text>
    </View>
    <View style={styles.buttonContainer}>
      <View style={styles.button}>
        <TouchableNativeFeedback
          background={TouchableNativeFeedback.Ripple('rgba(0,0,0,.1)', false)}
          onPress={() => {
            navigation.dispatch(navigateTo('ObservationMapScreen'));
            navigation.dispatch(resetStack('ObservationMapScreen'));
          }}
        >
          <View style={styles.buttonTextContainer}>
            <Text>{I18n.t('ok')}</Text>
          </View>
        </TouchableNativeFeedback>
      </View>
    </View>
  </View>
);

TaskCompletedScreen.propTypes = {
  navigation: PropTypes.shape({}).isRequired,
};

TaskCompletedScreen.navigationOptions = {
  header: null,
};

export default TaskCompletedScreen;
