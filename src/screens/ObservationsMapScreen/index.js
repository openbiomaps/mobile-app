import React, { Component } from 'react';
import {
  View,
  Text,
  TouchableWithoutFeedback,
  PermissionsAndroid,
  Alert,
  ActivityIndicator,
  TouchableHighlight,
} from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import Geolocation from 'react-native-geolocation-service';
import Menu from '../../components/ProfilePopupMenu';
import Popup from '../../components/MeasurementInfoPopup';
import { navigateTo, navigateBack, loadFormData, closeFirstUseHint } from '../../actions';
import I18n from '../../i18n';
import styles from './styles';
import { footerStyle, SignalIcon } from '../../components';
import { getCenterOfPlot } from '../../utils';
import MapComponent from '../../components/Map';

class ObservationMap extends Component {
  static navigationOptions = {
    header: null,
  }

  static propTypes = {
    navigation: PropTypes.shape({}).isRequired,
    navigateTo: PropTypes.func.isRequired,
    lastMeasurements: PropTypes.array,
    permanentSamplePlots: PropTypes.array,
    labels: PropTypes.array,
    observation: PropTypes.shape({}).isRequired,
    loadFormData: PropTypes.func.isRequired,
    fallbackPosition: PropTypes.shape({
      latitude: PropTypes.number.isRequired,
      longitude: PropTypes.number.isRequired,
    }).isRequired,
    hasErrors: PropTypes.any.isRequired,
    isFirstUse: PropTypes.bool,
    closeFirstUseHint: PropTypes.func,
    accessToken: PropTypes.string.isRequired,
    database: PropTypes.shape({}).isRequired,
  };

  static defaultProps = {
    lastMeasurements: [],
    permanentSamplePlots: [],
    labels: [],
    isFirstUse: false,
    closeFirstUseHint: null,
  };

  state = {
    indicator: true,
    showMenu: false,
    showPopup: false,
    selectedMeasurement: { values: {} },
  }

  async componentDidMount() {
    const {
      navigation,
      navigateBack,
    } = this.props;

    this.watchPosition();
    this.loadFormData();

    try {
      const granted =
        await PermissionsAndroid.request(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION);
      if (
        granted !== PermissionsAndroid.RESULTS.DENIED &&
        granted !== PermissionsAndroid.RESULTS.NEVER_ASK_AGAIN
      ) {
        this.getInitialPosition();
      } else {
        Alert.alert(I18n.t('error'), I18n.t('gps_disabled_hint'));
        navigation.dispatch(navigateBack());
      }
    } catch (err) {
      console.warn(err);
    }

    this.onNavigationFocus = navigation.addListener('willFocus', () => {
      this.watchPosition();
      this.loadFormData();
    });

    this.onNavigationBlur = navigation.addListener('willBlur', () => {
      Geolocation.clearWatch(this.watchId);
    });
  }

  componentWillUnmount() {
    this.onNavigationFocus.remove();
    this.onNavigationBlur.remove();
  }

  getInitialPosition = () => {
    this.setState({ error: I18n.t('gps_timeout') });
    const deltas = {
      latitudeDelta: 0.0082,
      longitudeDelta: 0.0051,
    };
    Geolocation.getCurrentPosition(({ coords }) => {
      const { longitude, latitude } = coords;
      const coordinate = { longitude, latitude };
      const region = {
        ...deltas,
        ...coordinate,
      };
      this.setState({
        region,
        position: coordinate,
        indicator: false,
        error: null,
      });
    }, () => {
      // fallback position if gps is unavailable
      // gets updated when gps is found
      const position = this.props.fallbackPosition;
      this.setState({
        region: {
          ...position,
          ...deltas,
        },
        error: I18n.t('gps_unavailable'),
        position,
        indicator: false,
      });
    }, {
      enableHighAccuracy: true,
      timeout: 20000,
      maximumAge: 10000,
    });
  }

  watchPosition = () => {
    Geolocation.clearWatch(this.watchId);
    this.watchId = Geolocation.watchPosition(
      ({ coords }) => {
        this.setState({
          position: coords,
          error: null,
        });
      },
      ({ code }) => {
        const error = code === 3 ? I18n.t('gps_timeout') : I18n.t('gps_unavailable');
        this.setState({ error });
        this.watchPosition();
      }, {
        timeout: 20000,
        maximumAge: 10000,
      },
    );
  }

  loadFormData = () => {
    const {
      accessToken,
      database,
      observation,
      loadFormData,
    } = this.props;

    loadFormData({
      projectName: database.name,
      accessToken,
      databaseListId: database.id,
      observationListId: observation.id,
      projectUrl: database.projectUrl,
    });
  }

  toggleMenu = () => {
    this.setState(prevState => ({ showMenu: !prevState.showMenu }));
  }

  showMessage = (message) => {
    this.setState({ message });
    setTimeout(() => {
      this.setState({ message: null });
    }, 5000);
  }

  togglePopup = () => {
    this.setState(prevState => ({ showPopup: !prevState.showPopup }));
  }

  handleObservationsPress = () => {
    const { navigation, navigateTo } = this.props;
    navigation.dispatch(navigateTo('ObservationCardsScreen'));
  }

  handleRegionChange = (region) => {
    this.setState({ region });
  }

  handleMeasurementPress = (measurement) => {
    this.setState({
      selectedMeasurement: measurement,
      showPopup: true,
    });
  }

  handleMyLocationPress = () => {
    this.map.animateToCoordinate(this.state.position, 300);
  }

  handleDonePress = () => {
    const { navigation, navigateTo, observation } = this.props;
    navigation.dispatch(navigateTo('CreateMeasurementScreen', {
      title: observation.name,
      onSave: this.showMessage,
    }));
  }

  handleHintPress = () => {
    this.props.closeFirstUseHint();
  }

  render() {
    const {
      lastMeasurements,
      permanentSamplePlots,
      labels,
      observation: { name, form },
      hasErrors,
    } = this.props;

    return (
      <View style={styles.container}>
        {
          form &&
          <Popup
            values={this.state.selectedMeasurement.values}
            dataTypes={form.dataTypes}
            observation={name}
            visible={this.state.showPopup}
            onClose={this.togglePopup}
          />
        }
        {this.state.indicator ?
          <ActivityIndicator /> :
          <MapComponent
            mapRef={(ref) => { this.map = ref; }}
            permanentSamplePlots={permanentSamplePlots}
            labels={labels}
            lastMeasurements={lastMeasurements}
            region={this.state.region}
            onRegionChange={this.handleRegionChange}
            onMeasurementPress={this.handleMeasurementPress}
            position={this.state.position}
          />
        }
        <View style={styles.buttonContainer}>
          <TouchableWithoutFeedback onPress={this.handleMyLocationPress}>
            <View style={styles.myLocationButton}>
              <Icon name="my-location" size={24} style={styles.myLocationIcon} />
            </View>
          </TouchableWithoutFeedback>
          <TouchableWithoutFeedback onPress={this.handleDonePress}>
            <View style={styles.createMeasurementButton}>
              <Icon name="assignment" size={24} style={styles.createMeasurementIcon} />
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={footerStyle.container}>
          <TouchableHighlight onPress={this.toggleMenu} underlayColor="rgba(0,0,0,.2)">
            <View>
              <Icon name="menu" size={24} color="black" style={footerStyle.button} />
            </View>
          </TouchableHighlight>
          <SignalIcon />
          <TouchableHighlight onPress={this.handleObservationsPress} underlayColor="rgba(0,0,0,.2)">
            <View>
              <Icon name={hasErrors ? 'report-problem' : 'remove-red-eye'} size={24} color="black" style={footerStyle.button} />
            </View>
          </TouchableHighlight>
        </View>
        {this.state.error &&
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>{this.state.error}</Text>
          </View>}
        {this.state.message &&
          <View style={styles.errorContainer}>
            <Text style={styles.errorText}>{this.state.message}</Text>
          </View>}
        <Menu
          visible={this.state.showMenu}
          onClose={this.toggleMenu}
          navigation={this.props.navigation}
          showNavigationOptions
        />
        {this.props.isFirstUse &&
          <TouchableWithoutFeedback onPress={this.handleHintPress}>
            <View style={styles.firstUseContainer}>
              <View style={styles.firstUseTextContainer}>
                <Text style={styles.firstUseText}>{I18n.t('create_measurement_hint')}</Text>
                <Icon color="white" name="arrow-forward" size={20} />
              </View>
            </View>
          </TouchableWithoutFeedback>
        }
      </View>
    );
  }
}

const mapStateToProps = ({ user: { accessToken, isFirstUse }, databases }) => {
  const selectedDatabase = databases.find(database => database.isSelected);
  const observation
    = selectedDatabase.observations.find(observation => observation.isSelected);

  const { form, measurements } = observation;

  const positionColumns = form ?
    form.dataTypes
      .filter(data => data.type === 'point' || data.type === 'wkt')
      .map(data => data.column) :
    [];
  const lastMeasurements = [];
  let hasErrors = false;

  if (measurements) {
    measurements.forEach((measurement) => {
      positionColumns.forEach((column) => {
        if (measurement.data[column]) {
          lastMeasurements.push({
            values: measurement.data,
            coordinate: measurement.data[column],
            id: measurement.id + column,
          });
        }
      });
    });

    hasErrors = measurements.filter(measurement => measurement.errors).length;
  }

  const locations = form ? form.permanentSamplePlots : [];

  const labels = locations.map(({
    name,
    data,
    type,
    id,
  }) => ({
    id,
    name,
    coordinate: getCenterOfPlot(type, data),
  }));

  // Budapest, if there aren't any permanent sample plots
  // or previous measurement positions available
  let fallbackPosition = { latitude: 47.4979, longitude: 19.0402 };
  if (labels.length) {
    fallbackPosition = labels[0].coordinate;
  } else if (lastMeasurements.length) {
    fallbackPosition = lastMeasurements[0].coordinate;
  }

  return ({
    accessToken,
    lastMeasurements,
    observation,
    database: selectedDatabase,
    permanentSamplePlots: locations,
    labels,
    fallbackPosition,
    hasErrors,
    isFirstUse,
  });
};

const mapDispatchToProps = {
  navigateTo,
  navigateBack,
  loadFormData,
  closeFirstUseHint,
};

export default connect(mapStateToProps, mapDispatchToProps)(ObservationMap);
