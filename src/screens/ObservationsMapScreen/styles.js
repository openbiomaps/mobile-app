import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  buttonContainer: {
    position: 'absolute',
    bottom: 20,
    right: 10,
  },
  myLocationIcon: {
    transform: [{ rotate: '-45deg' }],
    color: '#333',
  },
  myLocationButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 2,
  },
  createMeasurementIcon: {
    transform: [{ rotate: '-45deg' }],
    color: '#7EC667',
  },
  createMeasurementButton: {
    backgroundColor: 'white',
    margin: 15,
    padding: 10,
    transform: [{ rotate: '45deg' }],
    borderRadius: 10,
    elevation: 2,
  },
  container: {
    flex: 1,
    width: '100%',
    justifyContent: 'flex-end',
  },
  errorContainer: {
    position: 'absolute',
    backgroundColor: 'rgba(0, 0, 0, 0.87)',
    top: 0,
    left: 0,
    right: 0,
    padding: 10,
  },
  errorText: {
    color: 'white',
  },
  firstUseContainer: {
    backgroundColor: 'rgba(0,0,0,.5)',
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
  },
  firstUseTextContainer: {
    backgroundColor: 'rgba(0,0,0,.5)',
    position: 'absolute',
    height: 64,
    bottom: 25,
    right: 80,
    left: 0,
    padding: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  firstUseText: {
    color: 'white',
    fontSize: 16,
    flex: 1,
  },
});
