import React, { Component, Fragment } from 'react';
import { ScrollView, Text, TouchableWithoutFeedback, View } from 'react-native';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import { connect } from 'react-redux';
import { navigateTo, quizPassed } from '../../actions';
import { headerTitleStyle, titleStyle, headerStyle, footerStyle, sideButtonStyle } from '../../components';
import Question from './Question';
import I18n from '../../i18n';
import styles from './styles';

class QuizQuestionsScreen extends Component {
  static navigationOptions = ({ navigation }) => {
    const { state: { params } } = navigation;
    return {
      headerTitle: (
        <View style={headerTitleStyle.container}>
          <Text style={headerTitleStyle.title} numberOfLines={1}>{params ? `${params.title}` : ''}</Text>
          <Text style={headerTitleStyle.description}>{I18n.t('questions_screen_title')}</Text>
        </View>
      ),
      headerStyle,
      headerTitleStyle: titleStyle,
    };
  };

  static propTypes = {
    databaseId: PropTypes.string.isRequired,
    observationId: PropTypes.string.isRequired,
    navigation: PropTypes.shape({}).isRequired,
    navigateTo: PropTypes.func.isRequired,
    quizPassed: PropTypes.func.isRequired,
    questions: PropTypes.arrayOf(PropTypes.shape({
      caption: PropTypes.string.isRequired,
      answers: PropTypes.arrayOf(PropTypes.shape({
        text: PropTypes.string.isRequired,
        isRight: PropTypes.bool.isRequired,
      })).isRequired,
      qtype: PropTypes.string.isRequired,
    })).isRequired,
  };

  constructor(props) {
    super(props);
    this.state = {
      questions: props.questions.map(question => ({
        ...question,
        answers: question.answers.map(answer => ({
          ...answer,
          value: false,
        })),
      })),
      showError: false,
    };
  }

  handleDonePress = () => {
    const {
      databaseId, observationId, navigation, quizPassed, navigateTo,
    } = this.props;
    const { state: { params } } = navigation;
    const { questions } = this.state;
    const passed = questions.reduce((acc, question) =>
      acc && question.answers.reduce((answerAcc, answer) =>
        answerAcc && (!answer.isRight === !answer.value), true), true);
    if (passed) {
      quizPassed(databaseId, observationId);
      navigation.dispatch(navigateTo('QuizTaskScreen', { title: params.title }));
    } else {
      this.setState({ showError: true });
    }
  }

  handleValueChange = (questionIndex, answer, value) => {
    this.setState((prevState) => {
      const questions = [...prevState.questions];
      const answers = questions[questionIndex].answers.map(answer => (
        questions[questionIndex].qtype === 'singleselect'
          ? { ...answer, value: false }
          : { ...answer }));

      answers[answer].value = value;
      questions[questionIndex].answers = answers;
      return {
        questions,
        showError: false,
      };
    });
  }

  render() {
    return (
      <Fragment>
        {this.state.showError && <Text style={styles.error}>{I18n.t('quiz_error')}</Text>}
        <ScrollView contentContainerStyle={styles.container}>
          {this.state.questions.map((question, index) => (
            <Question
              {...question}
              key={question.caption}
              onValueChange={(answer, value) => this.handleValueChange(index, answer, value)}
              showErrors={this.state.showErrors}
            />
          ))}
        </ScrollView>
        <View style={sideButtonStyle.buttonContainer}>
          <TouchableWithoutFeedback onPress={() => this.handleDonePress()}>
            <View style={sideButtonStyle.doneButton}>
              <Icon name="done" size={24} style={sideButtonStyle.doneIcon} />
            </View>
          </TouchableWithoutFeedback>
        </View>
        <View style={footerStyle.container} />
      </Fragment>
    );
  }
}

const mapStateToProps = ({ databases }) => {
  const database = databases.find(database => database.isSelected);
  const observation = database.observations.find(observation => observation.isSelected);
  const { id, game: { questions } } = observation;
  return { databaseId: database.id, observationId: id, questions };
};

export default connect(
  mapStateToProps,
  { navigateTo, quizPassed },
)(QuizQuestionsScreen);
