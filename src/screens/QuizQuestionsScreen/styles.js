import { StyleSheet } from 'react-native';

export default StyleSheet.create({
  container: {
    paddingBottom: 35,
  },
  answerContainer: {
    borderBottomColor: '#e5e5e5',
    borderBottomWidth: 1,
    margin: 0,
    marginLeft: 15,
    flexDirection: 'row',
    paddingVertical: 8,
    alignItems: 'center',
    paddingRight: 5,
  },
  text: {
    flex: 1,
    fontSize: 14,
  },
  questionContainer: {
    paddingBottom: 10,
  },
  htmlContainer: {
    marginHorizontal: 15,
    marginVertical: 10,
  },
  error: {
    paddingLeft: 20,
    paddingVertical: 15,
    color: 'white',
    backgroundColor: '#f93636',
  },
});
