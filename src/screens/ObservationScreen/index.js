import { connect } from 'react-redux';
import I18n from '../../i18n';
import ObservationList from '../../components/ObservationList';
import { loadObservations, selectObservation, navigateTo } from '../../actions';

const mapStateToProps = ({ user: { accessToken }, databases }) => {
  const selectedDatabase = databases.find(database => database.isSelected);
  const origObservations = (selectedDatabase && selectedDatabase.observations) ?
    selectedDatabase.observations : [];

  const trainings = origObservations.filter(observation =>
    observation.game && observation.game.isEnabled);

  const enabledTrainings = trainings.filter(training => !training.game.isValidatedByBackend);

  enabledTrainings.sort((a, b) => a.game.qorder - b.game.qorder);

  let [currentTraining] = enabledTrainings;

  if (!enabledTrainings.length) {
    const maxQorder = Math.max(...trainings.map(({ game }) => game.qorder));
    currentTraining = trainings.find(({ game }) => Number(game.qorder) === maxQorder);
  }

  const observations = origObservations.map((observation) => {
    if (observation.game) {
      return {
        ...observation,
        description: I18n.t('game_observation'),
        disabled: observation.id !== (currentTraining && currentTraining.id),
      };
    }
    return selectedDatabase.game === 'on' && enabledTrainings.length
      ? { ...observation, disabled: true }
      : { ...observation };
  });
  return ({
    accessToken,
    observations,
    database: selectedDatabase,
  });
};


export default connect(
  mapStateToProps,
  { loadObservations, selectObservation, navigateTo },
)(ObservationList);

