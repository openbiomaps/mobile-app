import { connect } from 'react-redux';
import { loadDatabases, selectDatabase, navigateTo } from '../../actions';
import DatabaseList from '../../components/DatabaseList';

const mapStateToProps = ({ user: { accessToken }, server, databases }) => ({
  accessToken,
  server,
  databases: databases.filter(database => database.lastSelected).sort((a, b) =>
    new Date(b.lastSelected) - new Date(a.lastSelected)),
});

export default connect(
  mapStateToProps,
  { loadDatabases, selectDatabase, navigateTo },
)(DatabaseList);
