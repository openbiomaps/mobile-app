let navigator;

const setTopLevelNavigator = (navigatorRef) => {
  navigator = navigatorRef;
};

const dispatch = (args) => {
  navigator.dispatch(args);
};

export default {
  dispatch,
  setTopLevelNavigator,
};
