import { createSwitchNavigator } from 'react-navigation';
import SelectorNavigator from './navigators/SelectorNavigator';
import MainNavigator from './navigators/MainNavigator';
import LoginScreen from './screens/LoginScreen';

export default createSwitchNavigator(
  {
    Login: { screen: LoginScreen },
    Select: { screen: SelectorNavigator },
    Main: { screen: MainNavigator },
  },
  { initialRouteName: 'Login' },
  { headerMode: 'none' },
  {
    navigationOptions: {
      header: null,
    },
  },
);
