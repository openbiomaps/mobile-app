This is a client application for OpenBioMaps PDS API

This client app is able 
- to connect OBM servers
- to authenticate
- download forms
- upload observations
- work offline